﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;

namespace Server.Communication.Callback
{
    internal class CallbackSetDictionary : Dictionary<string, CallbackSet>, ICloneable
    {
        public CallbackSetDictionary()
            : this(4)
        {
        }

        public CallbackSetDictionary(IEqualityComparer<string> comparer)
            : this(4, comparer)
        {
        }

        public CallbackSetDictionary(int capacity, IEqualityComparer<string> comparer = (IEqualityComparer<string>)null)
            : base(capacity, comparer)
        {
        }

        /// <summary>
        /// Создает новый объект, являющийся копией текущего экземпляра.
        /// </summary>
        /// <returns>
        /// Новый объект, являющийся копией этого экземпляра.
        /// </returns>
        public object Clone()
        {
            var copy = new CallbackSetDictionary(Count);

            foreach (KeyValuePair<string, CallbackSet> pair in this)
            {
                copy.Add(pair.Key, pair.Value);
            }

            return copy;
        }
    }

    /// <summary>
    /// Содержит наборы колбеков. Для каждого клиента есть свой набор с доступными ему колбеками.
    /// </summary>
    internal class CallbackManager
    {
        //string - token
        private readonly CallbackSetDictionary _callbackSets;
        private readonly object _callbackSetLocker = new object();

        public CallbackManager()
        {
            _callbackSets = new CallbackSetDictionary();
        }

        /// <summary>
        /// Зарегистрировать набор колбеков для текущей сессии (авторизации), который
        /// хранит в себе набор колбеков. И сразу же колбек авторизации
        /// </summary>
        /// <param name="token"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public CallbackSet RegisterCallbackSet(string token, IAuthServiceCallback callback)
        {
            lock (_callbackSetLocker)
            {
                if (!_callbackSets.ContainsKey(token))
                {
                    CallbackSet set = new CallbackSet();
                    set.RegisterCallback(callback);
                    _callbackSets.Add(token, set);
                }
                else
                {
                    throw new CallbackExistsException();
                }

                return _callbackSets[token];
            }
        }

        public void UnregisterCallbackSet(string token)
        {
            lock (_callbackSetLocker)
            {
                _callbackSets.Remove(token);
            }
        }

        public CallbackSet GetCallbackSet(string token)
        {
            lock (_callbackSetLocker)
            {
                return _callbackSets[token];
            }
        }

        /// <summary>
        /// Отправить подключенным клиентам уведомление, что пользователь авторизовался
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="programId"></param>
        public void UserAuthorized(int userId, int programId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                try
                {
                    pair.Value.AuthCallback.UserAuthorized(userId, programId);
                }
                catch (ObjectDisposedException ex)
                {
                    pair.Value.RegisterCallback((IAuthServiceCallback)null);
                }
                catch (CommunicationException ex)
                {
                    AppendToLog("UserAuthorized. Нет связи с клиентом");
                    pair.Value.RegisterCallback((IAuthServiceCallback)null);
                }
            }
        }

        public void UserDisconnected(int userId, int programId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                try
                {
                    pair.Value.AuthCallback.UserDisconnected(userId, programId);
                }
                catch (ObjectDisposedException ex)
                {
                    pair.Value.RegisterCallback((IAuthServiceCallback)null);
                }
                catch (CommunicationException ex)
                {
                    AppendToLog("UserDisconnected. Нет связи с клиентом");
                    pair.Value.RegisterCallback((IAuthServiceCallback)null);
                }
            }

        }

        public void ClientAdded(int clientId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                if (pair.Value.ClientCallback != null)
                {
                    try
                    {
                        pair.Value.ClientCallback.ClientAdded(clientId);
                    }
                    catch (ObjectDisposedException ex)
                    {
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                    catch (CommunicationException ex)
                    {
                        AppendToLog("ClientAdded. Нет связи с клиентом");
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                }
            }

        }

        public void CarAdded(int carId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                if (pair.Value.CarCallback != null)
                {
                    try
                    {
                        pair.Value.CarCallback.CarAdded(carId);
                    }
                    catch (ObjectDisposedException ex)
                    {
                        pair.Value.RegisterCallback((ICarServiceCallback)null);
                    }
                    catch (CommunicationException ex)
                    {
                        AppendToLog("CarAdded. Нет связи с клиентом");
                        pair.Value.RegisterCallback((ICarServiceCallback)null);
                    }
                }
            }
        }

        public void ClientRemoved(int clientId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                if (pair.Value.ClientCallback != null)
                {
                    try
                    {
                        pair.Value.ClientCallback.ClientRemoved(clientId);
                    }
                    catch (ObjectDisposedException ex)
                    {
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                    catch (CommunicationException ex)
                    {
                        AppendToLog("ClientAdded. Нет связи с клиентом");
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                }
            }
        }

        public void ClientUpdated(int clientId)
        {
            CallbackSetDictionary sets;
            lock (_callbackSetLocker)
            {
                sets = (CallbackSetDictionary)_callbackSets.Clone();
            }

            foreach (KeyValuePair<string, CallbackSet> pair in sets)
            {
                if (pair.Value.ClientCallback != null)
                {
                    try
                    {
                        pair.Value.ClientCallback.ClientUpdated(clientId);
                    }
                    catch (ObjectDisposedException ex)
                    {
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                    catch (CommunicationException ex)
                    {
                        AppendToLog("ClientUpdated. Нет связи с клиентом");
                        pair.Value.RegisterCallback((IClientServiceCallback)null);
                    }
                }
            }
        }

        private void AppendToLog(string text)
        {
            //ToDo: Реализовать добавление в лог (CallbackManager.AppendToLog)
        }
    }
}
