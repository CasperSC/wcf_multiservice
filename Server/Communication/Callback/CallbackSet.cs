﻿using API.Communication.ServiceCallbacks;

namespace Server.Communication.Callback
{
    /// <summary>
    /// Представляет набор колбеков
    /// </summary>
    internal class CallbackSet
    {
        private IAuthServiceCallback _authCallback;
        private IClientServiceCallback _clientCallback;
        private ICarServiceCallback _carCallback;

        //public CallbackSet(string token)
        //{
        //    Token = token;
        //}

        public CallbackSet()
        {
            
        }

        //public string Token { get; set; }

        public IAuthServiceCallback AuthCallback
        {
            get { return _authCallback; }
        }

        public IClientServiceCallback ClientCallback
        {
            get { return _clientCallback; }
        }

        public ICarServiceCallback CarCallback
        {
            get { return _carCallback; }
        }

        public void RegisterCallback(IAuthServiceCallback callback)
        {
            _authCallback = callback;
        }

        public void RegisterCallback(IClientServiceCallback callback)
        {
            _clientCallback = callback;
        }

        public void RegisterCallback(ICarServiceCallback callback)
        {
            _carCallback = callback;
        }
    }
}