﻿using System;
using System.Collections.Generic;
using API.Data;
using Server.Communication.Callback;
using XXX.API.Communication.Auth;

namespace Server.Communication
{
    internal class Server
    {
        /// <summary>
        /// int - Id пользователя
        /// </summary>
        private readonly Dictionary<int, AuthResult> _authResults;

        private readonly object _sync = new object();

        static Server()
        {
            Instance = new Server();
        }

        public Server()
        {
            _authResults = new Dictionary<int, AuthResult>();
            CallbackManager = new CallbackManager();
        }

        public static Server Instance { get; private set; }

        public CallbackManager CallbackManager { get; private set; }

        /// <summary>
        /// Проверить по Id пользователя есть ли пользователь в списке авторизованных.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool IsUserAuthorized(int userId)
        {
            lock (_sync)
            {
                return _authResults.ContainsKey(userId);
            }
        }

        /// <summary>
        /// Авторизовать пользователя путём добавления его в словарь по ключу Id. Если в словаре уже есть 
        /// авторизованный пользователь под таким Id, то метод вернёт null.
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        public AuthResult Login(User user)
        {
            lock (_sync)
            {
                if (!_authResults.ContainsKey(user.Id))
                {
                    AuthResult auth = new AuthResult(user, GenerateToken());
                    _authResults.Add(user.Id, auth);
                    return auth;
                }

                return null;
            }
        }

        public void Logout(User user)
        {
            lock (_sync)
            {
                _authResults.Remove(user.Id);
            }
        }

        /// <summary>
        /// Сгенерировать маркер, уникальный для каждой сессии.
        /// </summary>
        /// <returns></returns>
        private string GenerateToken()
        {
            return Guid.NewGuid().ToString();
        }
    }
}