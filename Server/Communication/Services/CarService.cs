﻿using Server.Communication.Callback;
using System;
using System.Linq;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using Server.Models.DataInteraction;
using Car = API.Data.Car;

namespace Server.Communication.Services
{
    class CarService : ICarService
    {
        private ICarServiceCallback _callback;
        private readonly Database _database;
        private readonly Server _server;
        private int _userId;
        private string _token;

        public CarService()
        {
            _database = new Database();
            _server = Server.Instance;
        }

        public bool Add(Car car)
        {
            if (!CheckAccess())
                return false;

            int newId;
            bool result = _database.AddCar(DbModelConverter.ConvertCarToDbCar(car), out newId);

            _server.CallbackManager.CarAdded(newId);

            return result;
        }

        public Car[] GetAll()
        {
            return _database.GetAllCars().Select(DbModelConverter.ConvertDbCarToCar).ToArray();
        }

        public Car GetById(int id)
        {
            if (!CheckAccess())
                return null;

            return DbModelConverter.ConvertDbCarToCar(_database.GetCarById(id));
        }

        public bool Update(Car client)
        {
            throw new NotImplementedException();
        }

        public bool Connect(int userId, string token)
        {
            try
            {
                //Предполагается, что набор колбеков уже зарегистрирован сервисом авторизации для текущего клиента
                CallbackSet set = _server.CallbackManager.GetCallbackSet(token);
                if (set == null)
                    return false;

                _userId = userId;
                _token = token;

                ICarServiceCallback callback = OperationContext.Current.GetCallbackChannel<ICarServiceCallback>();
                set.RegisterCallback(callback);
                _callback = callback;

                return true;
            }
            catch (CallbackExistsException)
            {
                return false;
            }
        }

        private bool CheckAccess()
        {
            bool accessIsAllowed = false;

            if (_token != null)
            {
                accessIsAllowed = _server.IsUserAuthorized(_userId);
            }

            return accessIsAllowed;
        }
    }
}
