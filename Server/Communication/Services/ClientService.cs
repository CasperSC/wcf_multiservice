﻿using Server.Communication.Callback;
using System.Linq;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using Server.Models.DataInteraction;
using Client = API.Data.Client;

namespace Server.Communication.Services
{
    [ServiceBehavior(
     ConcurrencyMode = ConcurrencyMode.Multiple,
     InstanceContextMode = InstanceContextMode.PerSession,
     UseSynchronizationContext = false)]
    public class ClientService : IClientService
    {
        private IClientServiceCallback _callback;
        private readonly Database _database;
        private readonly Server _server;
        private int _userId;
        private string _token;

        public ClientService()
        {
            _database = new Database();
            _server = Server.Instance;
        }

        public bool Add(Client client)
        {
            if (!ServiceWasConnected())
                return false;

            int newId;
            bool result = _database.AddClient(DbModelConverter.ConvertClientToDbClient(client), out newId);

            _server.CallbackManager.ClientAdded(newId);

            return result;
        }

        public Client[] GetAll()
        {
            if (!ServiceWasConnected())
                return null;

            return _database.GetAllClients().Select(DbModelConverter.ConvertDbClientToClient).ToArray();
        }

        public Client GetById(int id)
        {
            if (!ServiceWasConnected())
                return null;

            return DbModelConverter.ConvertDbClientToClient(_database.GetClientById(id));
        }

        public bool Update(Client client)
        {
            if (!ServiceWasConnected())
                return false;


            bool result = _database.UpdateClient(DbModelConverter.ConvertClientToDbClient(client));
            if (result)
            {
                _server.CallbackManager.ClientUpdated(client.Id);
            }

            return result;
        }

        public bool Connect(int userId, string token)
        {
            try
            {
                //Предполагается, что набор колбеков уже зарегистрирован сервисом авторизации для текущего клиента
                CallbackSet set = _server.CallbackManager.GetCallbackSet(token);
                if (set == null)
                    return false;

                _userId = userId;
                _token = token;

                IClientServiceCallback callback = OperationContext.Current.GetCallbackChannel<IClientServiceCallback>();
                set.RegisterCallback(callback);
                _callback = callback;

                return true;
            }
            catch (CallbackExistsException)
            {
                return false;
            }
        }

        public bool RemoveClient(int id)
        {
            if (!ServiceWasConnected())
                return false;

            bool result = _database.RemoveClient(id);
            if (result)
            {
                _server.CallbackManager.ClientRemoved(id);
            }

            return result;
        }

        private bool ServiceWasConnected()
        {
            return _token != null;
        }
    }
}
