﻿using System;
using API.Communication.Auth;
using API.Data;
using Db = Server.Models.DataInteraction;

namespace Server.Communication.Services
{
    /// <summary>
    /// Конвертер из генерируемых инфраструктурой EntityFramework классов в API классы, которые используются 
    /// для передачи данных через WCF (DTO - Data Transfer Object).
    /// </summary>
    public static class DbModelConverter
    {
        public static User ConvertToUser(Db.User dbUser)
        {
            return new User {Id = dbUser.Id, Name = dbUser.Name, Permission = ConvertToPermission(dbUser.Permission)};
        }

        public static Permission ConvertToPermission(Db.PermissionLevel permission)
        {
            switch (permission)
            {
                case Db.PermissionLevel.None:
                    return Permission.None;
                case Db.PermissionLevel.User:
                    return Permission.User;
                case Db.PermissionLevel.Administrator:
                    return Permission.Administrator;
                default:
                    throw new ArgumentOutOfRangeException("permission");
            }
        }

        public static Db.PermissionLevel ConvertToDbPermission(Permission permission)
        {
            switch (permission)
            {
                case Permission.None:
                    return Db.PermissionLevel.None;
                case Permission.User:
                    return Db.PermissionLevel.User;
                case Permission.Administrator:
                    return Db.PermissionLevel.Administrator;
                default:
                    throw new ArgumentOutOfRangeException("permission");
            }
        }

        public static Db.Client ConvertClientToDbClient(Client client)
        {
            Db.Client dbClient = new Db.Client();

            dbClient.Id = client.Id;
            dbClient.Name = client.Name;
            dbClient.Address = client.Address;

            return dbClient;
        }

        public static Client ConvertDbClientToClient(Db.Client dbClient)
        {
            Client client = new Client();

            client.Id = dbClient.Id;
            client.Name = dbClient.Name;
            client.Address = dbClient.Address;

            return client;
        }

        public static Db.Car ConvertCarToDbCar(Car car)
        {
            return new Db.Car
            {
                Id = car.Id,
                Number = car.Number
            };
        }

        public static Car ConvertDbCarToCar(Db.Car car)
        {
            return new Car
            {
                Id = car.Id,
                Number = car.Number
            };
        }
    }
}