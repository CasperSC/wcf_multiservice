﻿using System;
using System.ServiceModel;
using API;
using API.Communication.Auth;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using MainClient.Utils;
using Server.Models.DataInteraction;
using XXX.API.Communication.Auth;

namespace Server.Communication.Services
{
    [ServiceBehavior(
         ConcurrencyMode = ConcurrencyMode.Multiple,
         InstanceContextMode = InstanceContextMode.PerSession,
         UseSynchronizationContext = false)]
    public class AuthService : IAuthService
    {
        private readonly Database _database;
        private readonly Server _server;
        private AuthResult _currentAuth;

        public AuthService()
        {
            _database = Database.Instance;
            _server = Server.Instance;
        }

        /// <summary>
        /// Авторизоваться. Если пользователь уже авторизован, пароли не совпали или такого пользователя не существует,
        /// метод вернёт null.
        /// </summary>
        /// <param name="authData"></param>
        /// <returns></returns>
        public AuthResult Login(AuthData authData)
        {
            var dbUser = _database.GetUserByName(authData.Login);

            if (dbUser != null && dbUser.Password == authData.Password)
            {
                var user = DbModelConverter.ConvertToUser(dbUser);

                if (!_server.IsUserAuthorized(user.Id))
                {
                    var authResult = _server.Login(user);
                    if (authResult == null)
                        return null;

                    try
                    {
                        var callback = OperationContext.Current.GetCallbackChannel<IAuthServiceCallback>();
                        _server.CallbackManager.RegisterCallbackSet(authResult.Token, callback);
                    }
                    catch (Exception ex) //CallbackExistsException
                    {
                        Logger.Current.AppendException(ex);
                    }

                    //ToDo: Сменить Constants.NewItemId на Id программы
                    _server.CallbackManager.UserAuthorized(authResult.User.Id, Constants.NewItemId);

                    _currentAuth = authResult;
                    return authResult;
                }
            }

            return null;
        }

        public void Logout()
        {
            LogoutInternal();
        }

        public bool CheckConnection()
        {
            return true;
        }

        public bool RegisterUser(string login, string password)
        {
            return _database.RegisterUser(login, password, DbModelConverter.ConvertToDbPermission(Permission.User));
        }

        public bool SetPermissionLevel(string login, Permission permission)
        {
            if (_currentAuth != null && _currentAuth.User.Permission == Permission.Administrator)
            {
                return _database.SetPermissionLevel(login, DbModelConverter.ConvertToDbPermission(permission));
            }

            return false;
        }

        ~AuthService()
        {
            LogoutInternal();
        }

        private void LogoutInternal()
        {
            if (_currentAuth != null)
            {
                _server.Logout(_currentAuth.User);

                //ToDo: Сменить Constants.NewItemId на Id программы
                _server.CallbackManager.UserDisconnected(_currentAuth.User.Id, Constants.NewItemId);
                _server.CallbackManager.UnregisterCallbackSet(_currentAuth.Token);
                _currentAuth = null;
            }
        }
    }
}
