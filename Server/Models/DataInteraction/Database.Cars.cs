﻿using System.Linq;
using Db = Server.Models.DataInteraction;

namespace Server.Models.DataInteraction
{
    internal partial class Database
    {
        public bool AddCar(Db.Car dbCar, out int carId)
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                var car = context.CarSet.Add(dbCar);
                bool success = context.SaveChanges() > 0;
                carId = car.Id;
                return success;
            }
        }

        public Db.Car[] GetAllCars()
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                return context.CarSet.ToArray();
            }
        }

        public Db.Car GetCarById(int id)
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                Db.Car car = context.CarSet.FirstOrDefault(c => c.Id == id);
                return car;
            }
        }

        public bool UpdateClient(Db.Car dbCar)
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                Db.Car car = context.CarSet.FirstOrDefault(c => c.Id == dbCar.Id);

                if (car != null)
                {
                    car = dbCar;
                    return context.SaveChanges() > 0;
                }

                return false;
            }
        }
    }
}
