﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Server.Models.DataInteraction
{
    internal partial class Database
    {
        #region Поля

        private static readonly Database _instance;
        private SynchronizationContext _syncContext;

        #endregion

        #region Конструкторы

        public Database()
        {
            _syncContext = SynchronizationContext.Current ?? new SynchronizationContext();
        }

        static Database()
        {
            _instance = new Database();
        }

        #endregion

        #region Свойства

        public static Database Instance
        {
            get { return _instance; }
        }

        #endregion

        public IEnumerable<User> GetUsers()
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                return context.UserSet.Select(user => user);
            }
        }

        /// <summary>
        /// Получить пользователя по имени, если не найден, вернёт null
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public User GetUserByName(string login)
        {
            using (DbModelContainer context = new DbModelContainer())
            {
                return context.UserSet.FirstOrDefault(user => user.Name == login);
            }
        }

        public bool GenerateUser(string name, PermissionLevel permission = PermissionLevel.User, string password = "")
        {
            using (var context = new DbModelContainer())
            {
                User user = new User();
                user.Name = name;
                user.Permission = permission;
                user.Password = password;

                context.UserSet.Add(user);
                return context.SaveChanges() > 0;
            }
        }

        public void RemoveUsers(IEnumerable<User> users)
        {
            using (var context = new DbModelContainer())
            {
                context.UserSet.RemoveRange(users);
                context.SaveChanges();
            }
        }

        public bool RegisterUser(string login, string password, PermissionLevel permission)
        {
            if (!string.IsNullOrWhiteSpace(login) && !string.IsNullOrWhiteSpace(password))
            {
                User resultUser = GetUserByLoginInternal(login);
                if (resultUser != null)
                {
                    return false;
                }

                var user = new User
                {
                    Name = login,
                    Password = password,
                    Permission = permission
                };
                using (var context = new DbModelContainer())
                {
                    context.UserSet.Add(user);
                    return context.SaveChanges() > 0;
                }
            }

            return false;
        }

        public bool SetPermissionLevel(string login, PermissionLevel permission)
        {
            using (var context = new DbModelContainer())
            {
                User resultUser = context.UserSet.FirstOrDefault(u => u.Name == login);
                if (resultUser != null)
                {
                    resultUser.Permission = permission;

                    context.SaveChanges();
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// Получить пользователя по имени из БД
        /// </summary>
        /// <param name="login">Логин/имя пользователя</param>
        /// <returns></returns>
        private User GetUserByLoginInternal(string login)
        {
            using (var context = new DbModelContainer())
            {
                return context.UserSet.FirstOrDefault(u => u.Name == login);
            }
        }
    }
}

//_dbContainer.Configuration.AutoDetectChangesEnabled = false;
