﻿using System.Data.Entity;
using System.Linq;
using Db = Server.Models.DataInteraction;

namespace Server.Models.DataInteraction
{
    internal partial class Database
    {
        public bool AddClient(Db.Client dbClient, out int clientId)
        {
            using (var context = new DbModelContainer())
            {
                var client = context.ClientSet.Add(dbClient);
                bool success = context.SaveChanges() > 0;
                clientId = client.Id;
                return success;
            }
        }

        public Db.Client[] GetAllClients()
        {
            using (var context = new DbModelContainer())
            {
                return context.ClientSet.Select(client => client).ToArray();
            }
        }

        public Db.Client GetClientById(int id)
        {
            using (var context = new DbModelContainer())
            {
                Db.Client client = context.ClientSet.FirstOrDefault(c => c.Id == id);
                return client;
            }
        }

        public bool UpdateClient(Db.Client dbClient)
        {
            using (var context = new DbModelContainer())
            {
                Db.Client client = context.ClientSet.Find(dbClient.Id);

                if (client != null)
                {
                    context.Entry(client).State = EntityState.Modified;
                    context.Entry(client).CurrentValues.SetValues(dbClient);
                    
                    return context.SaveChanges() > 0;
                }

                return false;
            }
        }

        public bool RemoveClient(int id)
        {
            using (var context = new DbModelContainer())
            {
                Db.Client client = context.ClientSet.FirstOrDefault(c => c.Id == id);

                if (client != null)
                {
                    context.ClientSet.Remove(client);
                    return context.SaveChanges() > 0;
                }

                return false;
            }
        }
    }
}
