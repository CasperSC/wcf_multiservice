﻿using Common.Communication;
using Server.Communication.Services;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using API.Communication.Services;
using Server.Models.DataInteraction;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceHost authHost = CreateServiceHost<IAuthService, AuthService>(Consts.AuthAddress);
            authHost.Open();

            ServiceHost clientHost = CreateServiceHost<IClientService, ClientService>(Consts.ClientsAddress);
            clientHost.Open();

            ServiceHost carHost = CreateServiceHost<ICarService, CarService>(Consts.CarsAddress);
            carHost.Open();

            Console.WriteLine("Сервисы запущены");

            Console.ReadKey();
        }

        private static ServiceHost CreateServiceHost<TContractType, TImplementationType>(string address)
            where TContractType : class
            where TImplementationType : class 
        {
            ServiceHost host = new ServiceHost(typeof(TImplementationType));
            var binding = new NetTcpBinding(SecurityMode.None);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            host.AddServiceEndpoint(typeof(TContractType), binding, address);

            return host;
        }

        private static void DisplayUsers(IEnumerable<User> users)
        {
            foreach (User user in users)
            {
                Console.WriteLine(user.Name);
            }
        }

        private static void GenerateUser()
        {
            bool genSuccess = Database.Instance.GenerateUser("Алексей", PermissionLevel.Administrator, "#4453fews4t$");
            if (genSuccess)
                Console.WriteLine("Данные сохранены");
            else
                Console.WriteLine("Не удалось сохранить данные");
        }
    }

    //Console.WriteLine("Создание пользователя");

    //DisplayUsers(DataManager.Instance.GetUsers());
    //Console.WriteLine();
    //Console.WriteLine("Удаляем одинаковых Алексеев");

    //IEnumerable<User> users = DataManager.Instance.GetUsers().Where(u => u.Name == "Алексей").Skip(1);

    //DataManager.Instance.RemoveUsers(users);
    //Console.WriteLine();
    //DisplayUsers(DataManager.Instance.GetUsers());
}
