﻿using System;
using System.Net;

namespace Common.Communication
{
    public class Consts
    {
        static Consts()
        {
            // A TCP error (10013: An attempt was made to access a socket in a way forbidden by its access permissions)
            // Нужно проверить используется ли уже порт, введя в командной строке: netstat -an | find "ПОРТ" 

            var uriBuilder1 = new UriBuilder("net.tcp", /*"localhost"*/Dns.GetHostName(), 8082, "AuthorizationService");
            AuthAddress = uriBuilder1.ToString();

            var uriBuilder2 = new UriBuilder("net.tcp", /*"localhost"*/Dns.GetHostName(), 8092, "ClientService");
            ClientsAddress = uriBuilder2.ToString();
                                                        
            var uriBuilder3 = new UriBuilder("net.tcp", /*"localhost"*/Dns.GetHostName(), 8083, "CarService");
            CarsAddress = uriBuilder3.ToString();
        }

        public static readonly string AuthAddress;

        public static readonly string ClientsAddress;

        public static readonly string CarsAddress;
    }
}
