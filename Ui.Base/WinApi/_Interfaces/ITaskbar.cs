using System.Drawing;

namespace Ui.Base.WinApi
{
    public interface ITaskbar
    {
        Rectangle Bounds { get; }

        TaskbarPosition Position { get; }

        Point Location { get; }

        /// <summary>
        ///     ������ ������ �����
        /// </summary>
        Size Size { get; }

        bool AlwaysOnTop { get; }

        /// <summary>
        ///     ���������� �� ������ ����� �������������
        /// </summary>
        bool AutoHide { get; }
    }
}