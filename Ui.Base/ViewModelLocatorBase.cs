﻿using System.Collections.Generic;
using System.Reflection;
using GalaSoft.MvvmLight;

namespace MainClient.ViewModel
{
    public abstract class ViewModelLocatorBase
    {
        /// <summary>
        /// Получить все модели представлений, которые находятся в свойствах данного экземпляра
        /// </summary>
        /// <returns></returns>
        public ViewModelBase[] GetViewModelTypes()
        {
            var properties = GetType().GetProperties();
            var viewModels = new List<ViewModelBase>(properties.Length);
            var vmBase = typeof(ViewModelBase);

            foreach (PropertyInfo propInfo in properties)
            {
                if (propInfo.PropertyType == vmBase || propInfo.PropertyType.IsSubclassOf(vmBase))
                {
                    var res = propInfo.GetValue(this);
                    viewModels.Add((ViewModelBase)res);
                }
            }
            return viewModels.ToArray();
        }

        protected virtual void AdditionalActionsBeforeCleanup()
        {
        }

        protected virtual void AdditionalActionsAfterCleanup()
        {
        }

        public void Cleanup()
        {
            AdditionalActionsBeforeCleanup();
            var viewModels = this.GetViewModelTypes();
            foreach (ViewModelBase viewModel in viewModels)
            {
                viewModel.Cleanup();
            }

            AdditionalActionsAfterCleanup();
        }
    }
}