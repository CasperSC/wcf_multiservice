using System.Windows;
using System.Xml.Serialization;

namespace Ui.Base.Windows.Configuration
{
    public class SerializablePoint
    {
        public SerializablePoint()
            : this(0d, 0d)
        {
        }

        public SerializablePoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>������</summary>
        [XmlAttribute]
        public double X { get; set; }

        /// <summary>������</summary>
        [XmlAttribute]
        public double Y { get; set; }

        /// <summary>
        /// ������� �������������� ����
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static implicit operator Point(SerializablePoint point)
        {
            return new Point(point.X, point.Y);
        }

        /// <summary>
        /// ������� �������������� ����
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        public static implicit operator SerializablePoint(Point point)
        {
            return new SerializablePoint(point.X, point.Y);
        }
    }
}