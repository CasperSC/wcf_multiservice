using System.Windows;
using System.Xml.Serialization;

namespace Ui.Base.Windows.Configuration
{
    public class SerializableSize
    {
        public SerializableSize()
            : this(0d, 0d)
        {
        }

        public SerializableSize(double width, double height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>������</summary>
        [XmlAttribute]
        public double Width { get; set; }

        /// <summary>������</summary>
        [XmlAttribute]
        public double Height { get; set; }

        /// <summary>
        /// ������� �������������� ����
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static implicit operator Size(SerializableSize size)
        {
            return new Size(size.Width, size.Height);
        }

        /// <summary>
        /// ������� �������������� ����
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static implicit operator SerializableSize(Size size)
        {
            return new SerializableSize(size.Width, size.Height);
        }
    }
}