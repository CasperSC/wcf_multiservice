using System.Windows;
using System.Xml.Serialization;

namespace Ui.Base.Windows.Configuration
{
    /// <summary>
    /// ��������� ����
    /// </summary>
    public class WindowSettings
    {
        public WindowSettings()
        {
            Size = new SerializableSize();
            Position = new SerializablePoint(-1d, -1d);
            WindowState = WindowState.Maximized;
        }

        /// <summary>������ ����</summary>
        [XmlElement(ElementName = "Size")]
        public SerializableSize Size { get; set; }

        /// <summary>������� ����</summary>
        [XmlElement(ElementName = "Position")]
        public SerializablePoint Position { get; set; }

        /// <summary>
        /// ��������� ����.
        /// </summary>
        [XmlAttribute("WindowState")]
        public WindowState WindowState { get; set; }


        [XmlIgnore]
        public bool IsDefault
        {
            get
            {
                return Size == new Size() && Position == new Point(-1d, -1d);
            }
        }
    }
}