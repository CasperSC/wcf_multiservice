﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using Ui.Base.WinApi;
using Ui.Base.Windows.Configuration;
using WinInterop = System.Windows.Interop;

namespace Ui.Base.Windows
{
    /// <summary>
    /// Базовый класс окон програмы, которые умеют запоминать свой размер, положение и последнее состояние.
    /// </summary>
    public class WindowBase : Window
    {
        private IHaveViewSettings _viewModel;
        //перед разворачиванием или сворачиванием окна необходимо запоминать предыдущий размер окна в нормальном состоянии.
        private Size? _prevSize;
        private WindowState? _prevState;

        private FrameworkElement _rootElement;
        private Thickness _marginOfElement;

        public WindowBase()
        {
            SourceInitialized += Window_SourceInitialized;
            Initialized += WindowBase_Initialized;
            Loaded += Window_Loaded;
            DataContextChanged += Window_DataContextChanged;
            ConsiderTheSizeOfTheTaskbar = true;
        }

        /// <summary>
        /// Возвращает или задаёт нужно ли учитывать размер панели задач.
        /// </summary>
        public bool ConsiderTheSizeOfTheTaskbar { get; set; }

        public WindowState? PrevState
        {
            get { return _prevState; }
        }

        private void WindowBase_Initialized(object sender, EventArgs e)
        {
            _rootElement = Content as FrameworkElement;
            if (_rootElement != null)
            {
                _marginOfElement = _rootElement.Margin;
                UpdateWindowState(); //нужно, если окно развёрнуто на весь экран изначально.
                //StateChanged += Window_StateChanged;
            }
        }

        private void UpdateWindowState()
        {
            _prevState = WindowState == WindowState.Minimized ? WindowState.Normal : WindowState;
            _rootElement.Margin = WindowState == WindowState.Maximized ? Margin : _marginOfElement;
        }

        private void Window_SourceInitialized(object sender, EventArgs e)
        {
            IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
            WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
        }

        private void UpdateWindowParameters(WindowSettings settings)
        {
            WindowStartupLocation = WindowStartupLocation.Manual;

            WindowState = settings.WindowState;
            Width = settings.Size.Width;
            Height = settings.Size.Height;
            Left = settings.Position.X;
            Top = settings.Position.Y;
        }

        private void SaveWindowParameters()
        {
            if (WindowState == WindowState.Normal)
            {
                _viewModel.WindowSettings.Size = new Size(Width, Height);
            }
            else if (WindowState == WindowState.Maximized || WindowState == WindowState.Minimized)
            {
                if (_prevSize != null)
                {
                    _viewModel.WindowSettings.Size = _prevSize.Value;
                }
            }
            _viewModel.WindowSettings.Position = new Point(Left, Top);

            WindowState? state = _prevState ?? (WindowState?)WindowState.Normal;
            _viewModel.WindowSettings.WindowState = WindowState == WindowState.Minimized ? state.Value : WindowState;
        }

        //В классе WindowsContainer в методе OpenWindow, если метод передать вью модель для этого окна
        //то окно визуализируется быстрее, чем выполняется код в методе Window_Loaded этого класса.
        //А так как в том методе мы меняем позицию и размер окна, то заметно, как оно резко перемещается после загрузки.
        //Зачем нужен дубль кода в Window_Loaded. Возможна ситуация, когда в метод OpenWindow класса WindowsContainer
        //не передадут вью модель, следовательно не сработает Window_DataContextChanged, но сработает Window_Loaded.
        private void Window_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            var viewModel = e.NewValue as IHaveViewSettings;
            if (viewModel == null)
            {
                DataContextChanged -= Window_DataContextChanged;
                Debug.WriteLine("DataContext окна не содержит реализацию интерфейса " + typeof(IHaveViewSettings).Name);
                return;
            }
            _viewModel = viewModel;

            if (_viewModel.WindowSettings != null)
            {
                WindowSettings settings = _viewModel.WindowSettings;
                if (!settings.IsDefault)
                {
                    UpdateWindowParameters(settings);
                }
            }
        }

        //-------------------------------------------------------------------------//

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            StateChanged += UpdateIfWindow_StateChanged;

            var viewModel = DataContext as IHaveViewSettings;
            if (viewModel == null)
            {
                UpdateWindowState();
                Loaded -= Window_Loaded;
                Debug.WriteLine("DataContext окна не содержит реализацию интерфейса " + typeof(IHaveViewSettings).Name);
                return;
            }
            _viewModel = viewModel;

            if (_viewModel.WindowSettings != null)
            {
                StateChanged += Window_StateChanged;
                IsVisibleChanged += WindowBase_IsVisibleChanged;
                Closing += Window_Closing;
                WindowSettings settings = _viewModel.WindowSettings;
                if (!settings.IsDefault)
                {
                    UpdateWindowParameters(settings);
                }
            }
            UpdateWindowState();
        }

        private void WindowBase_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            //Чтобы сохранялось положение, если окно вообще не закрывают, а только скрывают.
            if (!(bool)e.NewValue && _viewModel != null && _viewModel.WindowSettings != null)
            {
                SaveWindowParameters();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            SaveWindowParameters();
        }

        //Срабатывает, когда изменяется состояние окна (нормальное, свёрнутое, развернутое)
        private void Window_StateChanged(object sender, System.EventArgs e)
        {
            if (WindowState == WindowState.Normal)
            {
                _viewModel.WindowSettings.Size = new Size(Width, Height);
            }
            else if (WindowState == WindowState.Maximized || WindowState == WindowState.Minimized)
            {
                //если окно развернуть на весь экран и потом его свернуть, 
                //то будет попытка записать размеры уже не нормального состояния окна,
                //а развёрнутого, поэтому не сохраняем при сворачивании, если прошлое состояние - развёрнуто на весь экран.
                if (_prevState != null && _prevState == WindowState.Normal)
                {
                    Size size = new Size(ActualWidth, ActualHeight);
                    _prevSize = size;
                    _viewModel.WindowSettings.Size = size;
                }
            }

            if (WindowState != WindowState.Minimized)
            {
                _viewModel.WindowSettings.WindowState = WindowState;
            }

            _prevState = WindowState;
        }

        private void UpdateIfWindow_StateChanged(object sender, System.EventArgs e)
        {
            UpdateWindowState();
        }

        /// <summary>
        /// Обработчик очереди сообщений
        /// </summary>
        /// <param name="hwnd">A handle to the window.</param>
        /// <param name="msg">The message. For lists of the system-provided messages, see System-Defined Messages.</param>
        /// <param name="wParam">Additional message information. The contents of this parameter depend on the value of the uMsg parameter.</param>
        /// <param name="lParam">Additional message information. The contents of this parameter depend on the value of the uMsg parameter.</param>
        /// <param name="handled"></param>
        /// <returns></returns>
        private static IntPtr WindowProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            switch (msg)
            {
                case (int)WindowsMessage.WM_GETMINMAXINFO:
                    var hwndSource = WinInterop.HwndSource.FromHwnd(hwnd);
                    var window = (WindowBase)hwndSource.RootVisual;
                    WmGetMinMaxInfo(hwnd, lParam, window);
                    handled = true;
                    break;
            }

            return (IntPtr)0;
        }

        private static void WmGetMinMaxInfo(IntPtr hwnd, IntPtr lParam, WindowBase window)
        {
            MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

            // Adjust the maximized size and position to fit the work area of the correct monitor
            int MONITOR_DEFAULTTONEAREST = 0x00000002;
            IntPtr monitor = User32.MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

            if (monitor != IntPtr.Zero)
            {
                MONITORINFO monitorInfo = new MONITORINFO();
                User32.GetMonitorInfo(monitor, monitorInfo);
                RECT rcWorkArea = monitorInfo.rcWork;
                RECT rcMonitorArea = monitorInfo.rcMonitor;

                if (window.ConsiderTheSizeOfTheTaskbar)
                {
                    mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left);
                    mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top);
                    mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left);
                    mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top);
                }
                else
                {
                    mmi.ptMaxPosition.x = rcMonitorArea.left;
                    mmi.ptMaxPosition.y = rcMonitorArea.top;
                    mmi.ptMaxSize.x = rcMonitorArea.right;
                    mmi.ptMaxSize.y = rcMonitorArea.bottom;
                }

                mmi.ptMinTrackSize.x = (int)window.MinWidth;
                mmi.ptMinTrackSize.y = (int)window.MinHeight;
            }

            Marshal.StructureToPtr(mmi, lParam, true);
        }
    }
}