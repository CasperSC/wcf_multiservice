﻿using System;
using System.ServiceModel;
using System.Threading;
using API.Communication.ServiceCallbacks;

namespace ConsApp_Client.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class AuthServiceCallback : IAuthServiceCallback
    {
        private readonly Program _program;
        private readonly SynchronizationContext _context;

        public AuthServiceCallback(Program program, SynchronizationContext context)
        {
            _program = program;
            _context = context;
        }

        public void UserAuthorized(int userId, int programId)
        {
            Console.WriteLine(userId);
        }

        public void UserDisconnected(int userId, int programId)
        {
            Console.WriteLine(userId);
        }
    }
}
