﻿using System.ServiceModel;
using System.Threading;
using API.Communication.ServiceCallbacks;

namespace ConsApp_Client.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class ClientServiceCallback : IClientServiceCallback
    {
        private readonly Program _program;
        private readonly SynchronizationContext _context;

        public ClientServiceCallback(Program program, SynchronizationContext context)
        {
            _program = program;
            _context = context;
        }

        public void ClientAdded(int id)
        {
            //Task.Factory.StartNew(() =>
            //{
            _context.Post((unused) =>
            {
                _program.AddClient(id);
            }, null);
                
            //});
        }

        public void ClientRemoved(int clientId)
        {
            _context.Post((unused) =>
            {
                _program.ClientRemoved(clientId);
            }, null);
        }

        public void ClientUpdated(int clientId)
        {
            _context.Post((unused) =>
            {
                _program.ClientRemoved(clientId);
            }, null);
        }
    }
}
