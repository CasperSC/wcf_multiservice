﻿using Common.Communication;
using CommunicationLayer;
using ConsApp_Client.Communication.Callbacks;
using System;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using API.Communication.Auth;
using API.Communication.Services;
using API.Data;
using Utils.Security;
using XXX.API.Communication.Auth;

namespace ConsApp_Client
{
    class Program
    {
        static SynchronizationContext _syncContext = new SynchronizationContext();
        private IAuthService _authService;
        private IClientService _clientService;

        static void Main(string[] args)
        {
            Instance = new Program();
            Instance.Run();
        }

        public static Program Instance { get; private set; }

        /// <summary>Запустить работу программы</summary>
        private void Run()
        {
            _authService = InitAuthService();
            _clientService = InitClientService();

            Console.Write("Проверка соединения: ");
            Console.WriteLine(_authService.CheckConnection());

            AuthData auth = new AuthData() { Login = "CasperSC", Password = HashHelper.ComputeMd5Hash("123456") };
            //bool addSuccess = _authService.RegisterUser(auth.Login, auth.Password);
            //Console.WriteLine(addSuccess ? auth.Login + " успешно добавлен" : "Добавление не удалось");

            Console.WriteLine("Авторизация...");
            AuthResult result = _authService.Login(auth);
            Console.Write("Результат авторизации: ");
            if (result != null)
                Console.WriteLine("Id: {0}, Имя: {1}, Права доступа: {2}.", result.User.Id, result.User.Name, result.User.Permission);
            else
                Console.WriteLine("не удалась");

            const string userName = "Defiler";
            bool permsSetResult = _authService.SetPermissionLevel(userName, Permission.User);
            Console.WriteLine("Смена прав для пользователя \"{0}\" {1}.", userName,
                permsSetResult ? "успешно завершена" : "не удалась");

            if (result != null)
            {
                Console.WriteLine();
                Console.Write("Подключение нового сервиса: ");

                bool clientConnectResult = _clientService.Connect(result.User.Id, result.Token);
                if (clientConnectResult)
                {
                    _clientService.Add(new Client("Лукойл", "Не указан"));
                    _clientService.Add(new Client("Роял Фэктори", "Не указан"));
                }

                Console.WriteLine("{0}", clientConnectResult ? "Успешное подключение" : "Не удалось");
            }

            Console.ReadKey();

            _authService.Logout();
        }

        private static IAuthService InitAuthService()
        {
            EndpointAddress endpoint = new EndpointAddress(new Uri(Consts.AuthAddress));
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            IAuthService service = DuplexChannelFactory<IAuthService>.CreateChannel(
                new AuthServiceCallback(Program.Instance, _syncContext), binding, endpoint);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            return service;
        }

        private static IClientService InitClientService()
        {
            EndpointAddress endpoint = new EndpointAddress(new Uri(Consts.ClientsAddress));
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            IClientService service = DuplexChannelFactory<IClientService>.CreateChannel(
                new ClientServiceCallback(Program.Instance, _syncContext), binding, endpoint);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            return service;
        }

        public void AddClient(int id)
        {
            Task<Client>.Factory.StartNew(() => _clientService.GetById(id))
                .ContinueWith(task =>
                {
                    Client client = task.Result;
                    Console.WriteLine("Добавлен клиент: Id = {0}, Name = {1}, Address = {2}", client.Id,
                        client.Name,
                        client.Address);
                });
        }

        public void ClientRemoved(object id)
        {
            Console.WriteLine("Удалён клиент: Id = {0}");
        }
    }
}
