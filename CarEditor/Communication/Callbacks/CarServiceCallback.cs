﻿using System.ServiceModel;
using XXX.API.Communication.ServiceCallbacks;

namespace CarEditor.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class CarServiceCallback : ICarServiceCallback
    {
        public void CarAdded(int id)
        {
            
        }
    }
}