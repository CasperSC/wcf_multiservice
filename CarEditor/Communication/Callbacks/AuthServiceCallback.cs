﻿using System;
using System.ServiceModel;
using XXX.API.Communication.ServiceCallbacks;

namespace CarEditor.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class AuthServiceCallback : IAuthServiceCallback
    {
        //private readonly ServicesController _controller;
        //private readonly SynchronizationContext _context;

        public AuthServiceCallback()//ServicesController controller)
        {
            //_controller = controller;
        }

        public void UserAuthorized(int userId, int programId)
        {
            
        }

        public void UserDisconnected(int userId, int programId)
        {
            
        }
    }
}
