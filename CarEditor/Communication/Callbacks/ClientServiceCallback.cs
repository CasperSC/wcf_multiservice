﻿using System.ServiceModel;
using XXX.API.Communication.ServiceCallbacks;

namespace CarEditor.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class ClientServiceCallback : IClientServiceCallback
    {
        //private readonly ServicesController _controller;
        //private readonly SynchronizationContext _context;

        public ClientServiceCallback()//ServicesController controller)
        {
            //_controller = controller;
        }

        public void ClientAdded(int id)
        {
            //Task.Factory.StartNew(() =>
            //{

            //_controller.AddClient(id);

            //});
        }

        public void ClientRemoved(int id)
        {
        }
    }
}
