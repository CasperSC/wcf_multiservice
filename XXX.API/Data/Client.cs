﻿using System.Runtime.Serialization;

namespace API.Data
{
    /// <summary>Клиент</summary>
    [DataContract]
    public class Client : ICloneable<Client>
    {
        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        /// <param name="id">Идентификационный номер</param>
        /// <param name="name">Имя</param>
        /// <param name="address">Адрес</param>
        public Client(int id, string name, string address)
        {
            Id = id;
            Name = name;
            Address = address;
        }

        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="address">Адрес</param>
        public Client(string name, string address)
            : this(Constants.NewItemId, name, address)
        {
        }

        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        /// <param name="name">Имя клиента</param>
        public Client(string name)
            : this(Constants.NewItemId, name, string.Empty)
        {
        }

        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        public Client()
            : this(Constants.NewItemId, string.Empty, string.Empty)
        {
        }

        /// <summary>Идентификационный номер</summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>Имя</summary>
        [DataMember]
        public string Name { get; set; }

        /// <summary>Адрес</summary>
        [DataMember]
        public string Address { get; set; }

        #region Implementation of ICloneable<Client>

        /// <summary>
        /// Создать копию объекта
        /// </summary>
        /// <returns></returns>
        public Client Clone()
        {
            return new Client(Id, Name, Address);
        }

        #endregion
    }
}