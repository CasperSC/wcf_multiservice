﻿using System.Runtime.Serialization;

namespace API.Data
{
    /// <summary>Машина</summary>
    [DataContract]
    public class Car : ICloneable<Car>
    {
        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        /// <param name="id">Идентификационный номер</param>
        /// <param name="number">Номер машины</param>
        public Car(int id, string number)
        {
            Id = id;
            Number = number;
        }

        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        /// <param name="number">Номер машины</param>
        public Car(string number)
            : this(Constants.NewItemId, number)
        {
        }

        /// <summary>
        /// Инициализирует экземпляр класса
        /// </summary>
        public Car()
            : this(Constants.NewItemId, string.Empty)
        {
        }

        /// <summary>
        /// Идентификационный номер
        /// </summary>
        [DataMember]
        public int Id { get; set; }

        /// <summary>Номер машины</summary>
        [DataMember]
        public string Number { get; set; }

        #region Implementation of ICloneable<Car>

        /// <summary>
        /// Создать копию объекта
        /// </summary>
        /// <returns></returns>
        public Car Clone()
        {
            return new Car(Id, Number);
        }

        #endregion
    }
}
