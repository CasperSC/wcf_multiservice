﻿using System.Diagnostics;
using System.Runtime.Serialization;

namespace API.Data
{
    [DataContract]
    [DebuggerDisplay("Name = {Name}; Age = {Age}")]
    public class Person : ICloneable<Person>
    {
        public Person()
            : this(string.Empty)
        {
        }

        public Person(string name)
            : this(name, 0)
        {
        }

        public Person(string name, int age)
        {
            Age = age;
            Name = name;
        }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Age { get; set; }

        #region Implementation of ICloneable<Client>

        /// <summary>
        /// Создать копию объекта
        /// </summary>
        /// <returns></returns>
        public Person Clone()
        {
            return new Person(Name, Age);
        }

        #endregion

        public string GetFormattedText()
        {
            return string.Format("Имя: {0}, возраст: {1}", Name, Age);
        }
    }
}