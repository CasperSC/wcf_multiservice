﻿using System.Runtime.Serialization;
using API.Communication.Auth;

namespace API.Data
{
    [DataContract]
    public class User
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public Permission Permission { get; set; }
    }
}