﻿
namespace API
{
    /// <summary>
    /// Константы
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Id по умолчанию для новых элементов
        /// </summary>
        public const int NewItemId = -1;
    }
}
