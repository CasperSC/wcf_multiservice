using System.Collections.Generic;
using API.Data;

namespace API.Comparators
{
    public class EqualityComparerByClientId : IEqualityComparer<Client>
    {
        public bool Equals(Client x, Client y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode(Client obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}