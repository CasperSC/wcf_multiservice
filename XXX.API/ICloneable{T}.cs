﻿namespace API
{
    /// <summary>
    /// Предоставляет интерфейс для создания копии объекта.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICloneable<T>
    {
        /// <summary>
        /// Создать копию объекта.
        /// </summary>
        /// <returns></returns>
        T Clone();
    }
}