namespace API
{
    /// <summary>
    /// ������ ��� �������. ������������� ������ � ����� ������ �� ���.
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public interface IModelWrapper<out TModel> where TModel : ICloneable<TModel>
    {
        /// <summary>
        /// ���������� ����� ������ <see cref="TModel"/>.
        /// </summary>
        TModel ModelCopy { get; }
    }
}