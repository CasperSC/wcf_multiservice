﻿using System.ServiceModel;
using API.Communication.Auth;
using API.Communication.ServiceCallbacks;
using API.Data;
using XXX.API.Communication.Auth;

namespace API.Communication.Services
{
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IAuthServiceCallback))]
    public interface IAuthService
    {
        [OperationContract(IsInitiating = true, IsTerminating = false)]
        AuthResult Login(AuthData authData);

        //[OperationContract(IsInitiating = false, IsTerminating = true)]
        [OperationContract]
        void Logout();

        /// <summary>
        /// Проверить соединение
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool CheckConnection();

        [OperationContract]
        bool RegisterUser(string login, string password);

        /// <summary>
        /// Установить уровень доступа для пользователя. Изменить права может только администратор, в противном случае права изменены не будут.
        /// </summary>
        /// <param name="login">Логин/имя пользователя</param>
        /// <param name="permission">Новый уровень доступа</param>
        /// <returns></returns>
        [OperationContract]
        bool SetPermissionLevel(string login, Permission permission);
    }

    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(IClientServiceCallback))]
    public interface IClientService
    {
        [OperationContract]
        bool Add(Client client);

        [OperationContract]
        Client[] GetAll();

        [OperationContract]
        Client GetById(int id);

        [OperationContract]
        bool Update(Client client);

        [OperationContract]
        bool Connect(int userId, string token);

        [OperationContract]
        bool RemoveClient(int id);
    }

    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(ICarServiceCallback))]
    public interface ICarService
    {
        [OperationContract]
        bool Add(Car client);

        [OperationContract]
        Car[] GetAll();

        [OperationContract]
        Car GetById(int id);

        [OperationContract]
        bool Update(Car client);

        [OperationContract]
        bool Connect(int userId, string token);
    }
}
