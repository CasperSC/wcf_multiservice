﻿using System.Runtime.Serialization;

namespace API.Communication.Auth
{
    [DataContract]
    public enum Permission
    {
        [EnumMember]
        None,

        [EnumMember]
        User,

        [EnumMember]
        Administrator
    }
}