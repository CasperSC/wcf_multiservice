﻿using System.Runtime.Serialization;

namespace API.Communication.Auth
{
    [DataContract]
    public class AuthData
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }
    }
}
