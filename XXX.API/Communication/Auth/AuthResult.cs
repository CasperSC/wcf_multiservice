﻿using System.Runtime.Serialization;
using API.Data;

namespace XXX.API.Communication.Auth
{
    /// <summary>
    /// Результат авторизации, содержит в себе пользователя и маркер сессии
    /// </summary>
    [DataContract]
    public class AuthResult
    {
        public AuthResult()
        {
        }

        public AuthResult(User user, string token)
        {
            User = user;
            Token = token;
        }

        /// <summary>
        /// Пользователь
        /// </summary>
        [DataMember]
        public User User { get; set; }

        /// <summary>
        /// Маркер сессии
        /// </summary>
        [DataMember]
        public string Token { get; set; }
    }
}
