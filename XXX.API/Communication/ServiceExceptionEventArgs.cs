using System;

namespace API.Communication
{
    public class ServiceExceptionEventArgs
    {
        public ServiceExceptionEventArgs(Exception exception)
        {
            Exception = exception;
        }

        public Exception Exception { get; protected set; }
    }

    public class ConnectionEventArgs
    {
        public ConnectionEventArgs(bool success)
        {
            Success = success;
        }

        public bool Success { get; protected set; }
    }
}