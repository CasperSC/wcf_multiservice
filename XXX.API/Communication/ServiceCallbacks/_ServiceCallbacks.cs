﻿using System.ServiceModel;

namespace API.Communication.ServiceCallbacks
{
    [ServiceContract]
    public interface IAuthServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void UserAuthorized(int userId, int programId);

        [OperationContract(IsOneWay = true)]
        void UserDisconnected(int userId, int programId);
    }

    [ServiceContract]
    public interface IClientServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void ClientAdded(int id);

        [OperationContract(IsOneWay = true)]
        void ClientRemoved(int id);
        
        [OperationContract(IsOneWay = true)]
        void ClientUpdated(int id);
    }

    [ServiceContract]
    public interface ICarServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void CarAdded(int id);

        [OperationContract(IsOneWay = true)]
        void CarRemoved(int id);

        [OperationContract(IsOneWay = true)]
        void CarUpdated(int id);
    }
}
