﻿namespace API
{
    /// <summary>
    /// Предоставляет интерфейс для обновления модели из обёртки над моделью.
    /// </summary>
    /// <typeparam name="TModel">Тип модели</typeparam>
    public interface IModelUpdater<TModel>
    {
        /// <summary>
        /// Обновить модель
        /// </summary>
        /// <param name="model">Экземпляр модели, на основе которой необходимо обновить исходную модель.</param>
        void UpdateModel(TModel model);
    }
}