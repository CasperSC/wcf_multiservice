using System;
using System.Collections.Generic;
using System.IO;

namespace MainClient.Configuration
{
    internal class AppFolder
    {
        public const string DATA_FOLDER_NAME = "Data";
        public const string APP_DATA_FOLDER_NAME = "MultiService";
        public const string LOG_FOLDER_NAME = "Logs";

        private readonly Dictionary<Folder, string> _directories;

        public AppFolder()
        {
            string data = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), APP_DATA_FOLDER_NAME);
            string settings = Path.Combine(data, APP_DATA_FOLDER_NAME);
            string log = Path.Combine(data, LOG_FOLDER_NAME);

            _directories = new Dictionary<Folder, string>
            {               
                { Folder.Data, data },
                { Folder.Settings, settings },
                { Folder.Log, log },
            };
        }

        public string GetFolder(Folder folder)
        {
            return _directories[folder];
        }

        public void CheckFolders()
        {
            foreach (KeyValuePair<Folder, string> pair in _directories)
            {
                if (!Directory.Exists(pair.Value))
                {
                    Directory.CreateDirectory(pair.Value);
                }
            }
        }
    }
}