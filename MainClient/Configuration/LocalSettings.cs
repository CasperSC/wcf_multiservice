﻿using System;
using Ui.Base.Windows.Configuration;

namespace MainClient.Configuration
{
    [Serializable]
    public class LocalSettings
    {
        public LocalSettings()
        {
            MainWindowSettings = new WindowSettings();
        }

        /// <summary>
        /// Настройки для окна редактора клиентов
        /// </summary>
        public WindowSettings MainWindowSettings { get; set; }
    }
}
