﻿namespace MainClient.Configuration
{
    /// <summary>
    /// Обозначает папки приложения
    /// </summary>
    public enum Folder
    {
        /// <summary>
        /// Основная папка для хранения данных
        /// </summary>
        Data,

        /// <summary>
        /// Папка с настройками
        /// </summary>
        Settings,

        /// <summary>
        /// Парка логов приложения
        /// </summary>
        Log
    }
}