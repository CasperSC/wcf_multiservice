﻿using System.IO;

namespace MainClient.Configuration
{
    internal class AppFiles
    {
        /// <summary>
        /// Название файла настроек приложения
        /// </summary>
        public const string SETTINGS_FILE_NAME = "Settings.xml";

        /// <summary>
        /// Название файла логов приложения
        /// </summary>
        public const string LOG_FILE_NAME = "Log.txt";

        public AppFiles(AppFolder folders)
        {
            SettingsFile = Path.Combine(folders.GetFolder(Folder.Data), SETTINGS_FILE_NAME);
            LogFile = Path.Combine(folders.GetFolder(Folder.Log), LOG_FILE_NAME);
        }

        /// <summary>
        /// Путь к файлу настроек текущего приложения.
        /// </summary>
        public string SettingsFile { get; protected set; }

        /// <summary>
        /// Путь к файлу лога.
        /// </summary>
        public string LogFile { get; protected set; }
    }
}