﻿using System;
using System.IO;
using Utils.Data;

namespace MainClient.Configuration
{
    /// <summary>
    /// Взаимодействие с локальной конфигурацией программы
    /// </summary>
    internal class LocalConfiguration
    {
        private static readonly LocalConfiguration _instance;

        static LocalConfiguration()
        {
            _instance = new LocalConfiguration();
        }

        public LocalConfiguration()
        {
            Folders = new AppFolder();
            Files = new AppFiles(Folders);
        }

        public static LocalConfiguration Instance
        {
            get { return _instance; }
        }

        public AppFolder Folders { get; protected set; }

        public AppFiles Files { get; protected set; }

        public LocalSettings Settings { get; protected set; }

        public void SaveLocalSettings()
        {
            SaveObject(Settings, Files.SettingsFile);
        }

        public void LoadLocalSettings()
        {
            Settings = LoadObject<LocalSettings>(Files.SettingsFile);
        }

        private void SaveObject<T>(T serializableObject, string fileName)
        {
            try
            {
                DataSerializer<T>.SaveObject(serializableObject, fileName);
            }
            catch (DirectoryNotFoundException)
            {
                Folders.CheckFolders();
                try
                {
                    DataSerializer<T>.SaveObject(serializableObject, fileName);
                }
                catch (Exception)
                {
                }
            }
        }

        private T LoadObject<T>(string fileName) where T : class
        {
            T serializableObject;

            if (File.Exists(fileName))
            {
                try
                {
                    serializableObject = DataSerializer<T>.LoadObject(fileName);

                }
                catch (Exception ex)
                {
                    File.Delete(fileName);
                    serializableObject = (T)typeof(T).GetConstructors()[0].Invoke(null);
                }
            }
            else
            {
                serializableObject = (T)typeof(T).GetConstructors()[0].Invoke(null);
            }

            return serializableObject;
        }
    }
}