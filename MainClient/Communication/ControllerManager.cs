﻿using System;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;

namespace MainClient.Communication
{
    /// <summary>
    /// Менеджер контроллеров
    /// </summary>
    class ControllerManager
    {
        private static ControllerManager _instance;

        public ControllerManager(IAuthServiceController auth, ICarServiceController car, IClientServiceController client)
        {
            AuthController = auth;
            CarController = car;
            ClientController = client;
        }

        /// <summary>
        /// Возвращает или единожды задаёт единственный экземпляр класса в памяти.
        /// </summary>
        public static ControllerManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    throw new NullReferenceException("Менеджер контроллеров не проинициализирован");
                }
                return _instance;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                if (_instance != null)
                {
                    throw new InvalidOperationException("Нельзя изменить ранее проинициализированный экземпляр");
                }

                _instance = value;
            }
        }

        /// <summary>
        /// Контроллер, позволяющий работать с сервисом авторизации
        /// </summary>
        public IAuthServiceController AuthController { get; protected set; }

        /// <summary>
        /// Контроллер, позволяющий работать с сервисом автомобилей
        /// </summary>
        public ICarServiceController CarController { get; protected set; }

        /// <summary>
        /// Контроллер, позволяющий работать с сервисом клиентов
        /// </summary>
        public IClientServiceController ClientController { get; protected set; }
    }
}
