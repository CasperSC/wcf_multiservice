﻿using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using GalaSoft.MvvmLight.Threading;

namespace MainClient.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class ClientServiceCallback : IClientServiceCallback
    {
        private readonly IClientServiceCallback _handler;

        public ClientServiceCallback(IClientServiceCallback handler)
        {
            _handler = handler;
        }

        public void ClientAdded(int id)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.ClientAdded(id);
            });
        }

        public void ClientRemoved(int clientId)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.ClientRemoved(clientId);
            });
        }

        public void ClientUpdated(int clientId)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.ClientUpdated(clientId);
            });
        }
    }
}

