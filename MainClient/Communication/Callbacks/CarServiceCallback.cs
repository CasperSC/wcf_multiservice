﻿using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using GalaSoft.MvvmLight.Threading;

namespace MainClient.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    internal class CarServiceCallback : ICarServiceCallback
    {
        private readonly ICarServiceCallback _handler;

        public CarServiceCallback(ICarServiceCallback handler)
        {
            _handler = handler;
        }
        public void CarAdded(int id)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.CarAdded(id);
            });
        }

        public void CarRemoved(int id)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.CarRemoved(id);
            });
        }

        public void CarUpdated(int id)
        {
            DispatcherHelper.UIDispatcher.InvokeAsync(() =>
            {
                _handler.CarUpdated(id);
            });
        }
    }
}