﻿using System;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using GalaSoft.MvvmLight.Threading;

namespace MainClient.Communication.Callbacks
{
    [ServiceContract(SessionMode = SessionMode.Required)]
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, UseSynchronizationContext = false)]
    class AuthServiceCallback : IAuthServiceCallback
    {
        private readonly IAuthServiceCallback _handler;

        public AuthServiceCallback(IAuthServiceCallback handler)
        {
            _handler = handler;
        }

        public void UserAuthorized(int userId, int programId)
        {
            //Так сделано, потому что в определённых обстоятельствах клиент зависает.
            //Долгая история, много всего попробовано, это вполне себе решение.
            DispatcherHelper.UIDispatcher.BeginInvoke(new Action(() => 
            {
                _handler.UserAuthorized(userId, programId);
            }));
        }

        public void UserDisconnected(int userId, int programId)
        {
            DispatcherHelper.UIDispatcher.BeginInvoke(new Action(() =>
            {
                _handler.UserDisconnected(userId, programId);
            }));
        }
    }
}
