﻿/*
  In App.xaml:
  <Application.Resources>
      <vm:ViewModelLocator xmlns:vm="clr-namespace:MainClient"
                           x:Key="Locator" />
  </Application.Resources>
  
  In the View:
  DataContext="{Binding Source={StaticResource Locator}, Path=ViewModelName}"

  You can also use Blend to do all this with the tool's support.
  See http://www.galasoft.ch/mvvm
*/

using System;
using System.Windows;
using System.Windows.Threading;
using Autofac;
using CommunicationLayer.DataInteraction.Controllers;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Controls.Editors.CarEditor.ViewModel;
using Controls.Editors.ClientEditor.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Threading;
using MainClient.Communication;
using MainClient.Configuration;
using MainClient.Utils;
using Utils.Interfaces;
using Utils.Logging;
using Utils.Logging.Implementation;

namespace MainClient.ViewModel
{
    /// <summary>
    /// This class contains static references to all the view models in the
    /// application and provides an entry point for the bindings.
    /// </summary>
    public class ViewModelLocator : ViewModelLocatorBase
    {
        private readonly IContainer _container;

        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
            if (ViewModelBase.IsInDesignModeStatic)
            {
                return;
            }

            Logger.Current = new FileLogger(
                LocalConfiguration.Instance.Folders.GetFolder(Folder.Log),
                LocalConfiguration.Instance.Files.LogFile);

            Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;
            Application.Current.Exit += Current_Exit;

            DispatcherHelper.Initialize();

            LocalConfiguration.Instance.Folders.CheckFolders();
            LocalConfiguration.Instance.LoadLocalSettings();

            IContainer container = RegisterDependencies(LocalConfiguration.Instance);
            _container = container;

            ControllerManager.Instance = container.Resolve<ControllerManager>();

            Main.SimpleLogger = SimpleLogger;
        }

        private IContainer RegisterDependencies(LocalConfiguration config)
        {
            //http://autofac.org
            var builder = new ContainerBuilder();

            builder.RegisterType<MessageExceptionFormatter>()
                   .As<IExceptionFormatter>();

            builder.RegisterType<DebugLogger>()
                .As<ILogger>()
                .SingleInstance();

            builder.RegisterType<AuthServiceController>()
                .As<IAuthServiceController>()
                .SingleInstance();
            builder.RegisterType<CarServiceController>()
                .As<ICarServiceController>()
                .SingleInstance();
            builder.RegisterType<ClientServiceController>()
                .As<IClientServiceController>()
                .SingleInstance();

            builder.RegisterType<SimpleLoggerVm>()
                .SingleInstance();
            builder.Register(c => new CarEditorVm { CarController = c.Resolve<ICarServiceController>() });
            builder.Register(c => new ClientEditorVm { ClientController = c.Resolve<IClientServiceController>() });

            builder.Register(c => new MainViewModel
            {
                CarEditor = c.Resolve<CarEditorVm>(),
                ClientEditor = c.Resolve<ClientEditorVm>()
            }).OnActivated(e =>
                {
                    e.Instance.WindowSettings = config.Settings.MainWindowSettings; //Настройки окна (положение, размер, состояние)
                }).SingleInstance();
            
            builder.RegisterType<ControllerManager>();

            return builder.Build();
        }

        public MainViewModel Main
        {
            get { return _container.Resolve<MainViewModel>(); }
        }

        public SimpleLoggerVm SimpleLogger
        {
            get { return _container.Resolve<SimpleLoggerVm>(); }
        }

        private void Current_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            Logger.Current.AppendException(e.Exception);
            MessageBox.Show(e.Exception.ToString());
            Cleanup();
        }

        private void Current_Exit(object sender, ExitEventArgs e)
        {
            try
            {
                LocalConfiguration.Instance.SaveLocalSettings();
            }
            catch (Exception ex)
            {
                Logger.Current.AppendException(ex);
            }
        }

        public void Cleanup()
        {
            Main.Cleanup();
            if (SimpleLogger != null)
            {
                SimpleLogger.Cleanup();
            }
            _container.Dispose();
        }
    }
}