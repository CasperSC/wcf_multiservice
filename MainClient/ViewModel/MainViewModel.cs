﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using API.Communication.Auth;
using API.Communication.ServiceCallbacks;
using API.Data;
using Common.Communication;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Controls.Editors.CarEditor.ViewModel;
using Controls.Editors.ClientEditor.ViewModel;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using MainClient.Communication;
using MainClient.Communication.Callbacks;
using Ui.Base.Windows.Configuration;
using Utils.Security;
using XXX.API.Communication.Auth;

namespace MainClient.ViewModel
{
    public class MainViewModel : ViewModelBase, IAuthServiceCallback, IHaveViewSettings
    {
        private AuthResult _auth;
        private CarEditorVm _carEditor;
        private ClientEditorVm _clientEditor;
        private RelayCommand _connectToCarService;
        private RelayCommand _connectToClientService;
        private RelayCommand _disconnectFromCarService;
        private RelayCommand _disconnectFromClientService;
        private bool _isCarServiceConnected;
        private bool _isClientServiceConnected;
        private RelayCommand _loginCommand;
        private RelayCommand _logoutCommand;
        private AuthData _selectedAuthData;
        private bool _isLogined;

        /// <summary>
        ///     Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (!IsInDesignMode)
            {
                //IEnumerable<ClientVm> elements = GenerateClients().Select(p => new ClientVm(p));

                AuthData[] authDatas =
                {
                    //Переданый хэш по незащищённому каналу автоматически становится паролем.
                    //e10adc3949ba59abbe56e057f20f883e
                    new AuthData {Login = "CasperSC", Password = HashHelper.ComputeMd5Hash("123456")},
                    new AuthData {Login = "Reflector", Password = HashHelper.ComputeMd5Hash("123456")}
                };
                AuthDatas = new ObservableCollection<AuthData>(authDatas);
            }
        }

        #region Свойства
        
        /// <summary>
        /// Настройки окна
        /// </summary>
        public WindowSettings WindowSettings { get; set; }

        /// <summary>
        /// Возвращает или задаёт авторизован ли пользователь
        /// </summary>
        public bool IsLogined
        {
            get { return _isLogined; }
            set
            {
                _isLogined = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Модель представление редактора клиентов. 
        /// </summary>
        public ClientEditorVm ClientEditor
        {
            get { return _clientEditor; }
            set
            {
                _clientEditor = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Модель представление редактора автомобилей.
        /// </summary>
        public CarEditorVm CarEditor
        {
            get { return _carEditor; }
            set
            {
                _carEditor = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Логер, отображающий сообщения в UI.
        /// </summary>
        public SimpleLoggerVm SimpleLogger { get; set; }

        /// <summary>
        /// Выбранные данные для авторизации.
        /// </summary>
        public AuthData SelectedAuthData
        {
            get { return _selectedAuthData; }
            set
            {
                _selectedAuthData = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Коллекция данных авторизации.
        /// </summary>
        public ObservableCollection<AuthData> AuthDatas { get; set; }

        #endregion

        #region Команды

        /// <summary>
        /// Авторизоваться.
        /// </summary>
        public RelayCommand LoginCommand
        {
            get
            {
                return _loginCommand ?? (_loginCommand = new RelayCommand(async () =>
                {
                    if (SelectedAuthData != null && !IsLogined)
                    {
                        _auth = await Login(SelectedAuthData);
                        IsLogined = _auth != null;
                    }
                }));
            }
        }

        /// <summary>
        /// Выйти из аккаунта.
        /// </summary>
        public RelayCommand LogoutCommand
        {
            get
            {
                return _logoutCommand ?? (_logoutCommand = new RelayCommand(() =>
                    {
                        IAuthServiceController authService = ControllerManager.Instance.AuthController;
                        if (authService.IsServiceAlive)
                        {
                            authService.Logout();
                            ClientEditor.Clear();
                            CarEditor.Clear();
                            IsLogined = false;
                            SimpleLogger.AppendText("Произведён выход");
                        }
                        else
                        {
                            SimpleLogger.AppendText("Пользователь не авторизован");
                        }
                    }));
            }
        }

        /// <summary>
        /// Возвращает или задаёт, подключен ли сервис по работе с клиентами.
        /// </summary>
        public bool IsClientServiceConnected
        {
            get { return _isClientServiceConnected; }
            set
            {
                _isClientServiceConnected = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Возвращает или задаёт, подключен ли сервис по работе с автомобилями.
        /// </summary>
        public bool IsCarServiceConnected
        {
            get { return _isCarServiceConnected; }
            set
            {
                _isCarServiceConnected = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Подключиться к сервису клиентов.
        /// </summary>
        public RelayCommand ConnectToClientServiceCommand
        {
            get
            {
                return _connectToClientService ?? (_connectToClientService = new RelayCommand(
                    () =>
                    {
                        IAuthServiceController authController = ControllerManager.Instance.AuthController;
                        if (authController.IsServiceAlive)
                        {
                            if (_auth == null)
                            {
                                SimpleLogger.AppendText("ConnectToClientServiceCommand. _auth == null");
                                return;
                            }
                            IClientServiceController clientController = ControllerManager.Instance.ClientController;
                            clientController.InitializeService(Consts.ClientsAddress, new ClientServiceCallback(ClientEditor));
                            IsClientServiceConnected = clientController.SubscribeCallback(_auth);
                            if (IsClientServiceConnected)
                            {
                                SimpleLogger.AppendText("Сервис для работы с клиентами подключен");
                                ClientEditor.UpdateClients();
                            }
                        }
                    }));
            }
        }

        /// <summary>
        /// Отключиться от сервиса клиентов.
        /// </summary>
        public RelayCommand DisconnectFromClientServiceCommand
        {
            get
            {
                return _disconnectFromClientService ?? (_disconnectFromClientService = new RelayCommand(
                    () =>
                    {
                        ControllerManager.Instance.ClientController.CloseService();
                        IsClientServiceConnected = false;
                        SimpleLogger.AppendText("Сервис для работы с клиентами отключен");
                    }));
            }
        }

        /// <summary>
        /// Подключиться к сервису по работе с автомобилями.
        /// </summary>
        public RelayCommand ConnectToCarServiceCommand
        {
            get
            {
                return _connectToCarService ?? (_connectToCarService = new RelayCommand(
                    () =>
                    {
                        IAuthServiceController authController = ControllerManager.Instance.AuthController;
                        if (authController.IsServiceAlive)
                        {
                            if (authController.CheckConnection())
                            {
                                ICarServiceController carController = ControllerManager.Instance.CarController;
                                carController.InitializeService(Consts.CarsAddress, new CarServiceCallback(CarEditor));
                                IsCarServiceConnected = carController.SubscribeCallback(_auth);
                                if (IsCarServiceConnected)
                                {
                                    SimpleLogger.AppendText("Сервис для работы с клиентами подключен");
                                    //CarEditor.UpdateCars();
                                }
                            }
                        }
                    }));
            }
        }

        /// <summary>
        /// Отключиться от сервиса автомобилей.
        /// </summary>
        public RelayCommand DisconnectFromCarServiceCommand
        {
            get
            {
                return _disconnectFromCarService ?? (_disconnectFromCarService = new RelayCommand(
                    () =>
                    {
                        ControllerManager.Instance.CarController.CloseService();
                        IsCarServiceConnected = false;
                        SimpleLogger.AppendText("Сервис для работы с автомобилями отключен");
                    }));
            }
        }

        #endregion

        private Task<AuthResult> Login(AuthData authData)
        {
            return Task<AuthResult>.Factory.StartNew(() =>
             {
                 IAuthServiceController authController = ControllerManager.Instance.AuthController;
                 if (!authController.IsServiceAlive)
                 {
                     authController.InitializeService(Consts.AuthAddress, new AuthServiceCallback(this));
                 }

                 AuthResult auth = authController.Login(authData);

                 Debug.WriteLine(auth != null ? auth.User.Name : "Не удалось получить пользователя");
                 
#if DEBUG 
                 if (auth == null)
                 {
                     foreach (AuthData data in AuthDatas)
                     {
                         authController.RegisterUser(data.Login, data.Password);
                     }

                     auth = authController.Login(authData);
                     Debug.WriteLine(auth != null
                         ? auth.User.Name
                         : "Не удалось получить пользователя после добавления пользователей в БД");
                 }
#endif

                 return auth;
             });
        }

        private void AuthController_ConnectionStateChanged(object sender, bool state)
        {
            Debug.WriteLine("AuthController_ConnectionState = {0}", state);
            if (!state)
            {
                _auth = null;
                IsClientServiceConnected = false;
                IsCarServiceConnected = false;
            }
        }

        private Client[] GenerateClients(int quantity = 10)
        {
            var clients = new Client[quantity];
            for (int i = 0; i < quantity; i++)
            {
                clients[i] = new Client("Имя " + i, "неизвестен");
            }

            return clients;
        }

        /// <summary>
        ///     Начало инициализации программы
        /// </summary>
        public void StartInitialization()
        {
            IAuthServiceController authService = ControllerManager.Instance.AuthController;
            authService.ConnectionStateChanged += AuthController_ConnectionStateChanged;

            try
            {
                authService.InitializeService(Consts.AuthAddress, new AuthServiceCallback(this));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        ///     Уведомить, что пользователь авторизован
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="programId"></param>
        public void UserAuthorized(int userId, int programId)
        {
            string text = string.Format("Пользователь авторизован: Id = {0}, ProgramId = {1}", userId, programId);
            Debug.WriteLine(text);
            SimpleLogger.AppendText(text);
        }

        /// <summary>
        ///     Уведомить, что пользователь отключен
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="programId"></param>
        public void UserDisconnected(int userId, int programId)
        {
            string text = string.Format("Пользователь отключен: Id = {0}, ProgramId = {1}", userId, programId);
            Debug.WriteLine(text);
            SimpleLogger.AppendText(text);
        }

        /// <summary>
        /// Unregisters this instance from the Messenger class.
        /// <para>
        /// To cleanup additional resources, override this method, clean
        ///             up and then call base.Cleanup().
        /// </para>
        /// </summary>
        public override void Cleanup()
        {
            IAuthServiceController authService = ControllerManager.Instance.AuthController;
            authService.ConnectionStateChanged -= AuthController_ConnectionStateChanged;
            base.Cleanup();
        }
    }
}