﻿using System;
using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;
using Utils.Interfaces;
using Utils.Logging;

namespace MainClient.ViewModel
{
    public class SimpleLoggerVm : ViewModelBase, ILogger
    {
        private readonly IExceptionFormatter _formatter;
        private readonly ObservableCollection<string> _internalMessages;

        public SimpleLoggerVm()
            : this(null)
        {
        }

        public SimpleLoggerVm(IExceptionFormatter formatter)
        {
            _formatter = formatter;
            _internalMessages = new ObservableCollection<string>();
            Messages = new ReadOnlyObservableCollection<string>(_internalMessages);
        }

        public ReadOnlyObservableCollection<string> Messages { get; private set; }

        /// <summary>
        /// Добавить текст в конец файла лога, добавив в начале дату и время с точностью до миллесекунд в начале текста
        /// </summary>
        /// <param name="text"></param>
        public void AppendText(string text)
        {
            _internalMessages.Add(text);
        }

        public void AppendException(Exception ex)
        {
            if (_formatter == null)
            {
                _internalMessages.Add("Исключение: " + ex.Message);
            }
            else
            {
                _internalMessages.Add("Исключение: " + _formatter.Format(ex));
            }
        }
    }
}
