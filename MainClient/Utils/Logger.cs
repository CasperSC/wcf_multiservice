﻿using System;
using Utils.Logging;
using Utils.Logging.Implementation;

namespace MainClient.Utils
{
    /// <summary>
    /// Содержит в себе статическое свойство, ссылающееся на логер, который 
    /// нужно устанаваливать самостоятельно.
    /// </summary>
    public class Logger
    {
        private Logger()
        {
        }

        private static ILogger _logger;

        /// <summary>
        /// Возвращает или задаёт единственный экземпляр класса в памяти. По умолчанию создаёт <see cref="DebugLogger"/>.
        /// При присвоении нового значения, если текущий экземпляр логера реализует интерфейс <see cref="IDisposable"/>, то 
        /// вызывает метод Dispose и только после этого присваивает новый экземпляр реализации интерфейса <see cref="ILogger"/>.
        /// <exception cref="ArgumentNullException">При передаче null значения.</exception>
        /// </summary>
        public static ILogger Current
        {
            get { return _logger ?? (_logger = new DebugLogger(new MessageExceptionFormatter())); }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }

                IDisposable disposable = _logger as IDisposable;
                if(disposable != null)
                {
                    disposable.Dispose();
                }

                _logger = value;
            }
        }
    }
}
