﻿using System;
using Utils.Interfaces;

namespace MainClient.Utils
{
    /// <summary>
    /// Класс, предоставляющий доступ к методу форматирования исключения в строку
    /// </summary>
    internal class MessageExceptionFormatter : IExceptionFormatter
    {
        /// <summary>
        /// Форматирует исключение в строку, которое включает в себя сообщение и источник исключения
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public string Format(Exception ex)
        {
            return string.Format("Message: {0}; {1}", ex.Message, ex.Source);
        }
    }
}