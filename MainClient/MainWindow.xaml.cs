﻿using System.Diagnostics;
using System.Windows;
using MainClient.ViewModel;
using Ui.Base.Windows;

namespace MainClient
{
    public partial class MainWindow : CustomWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModelLocator locator = (ViewModelLocator)Application.Current.FindResource("Locator");
            Debug.Assert(locator != null, "Ресурс \"Locator\" не найден");
            locator.Main.StartInitialization();
        }
    }
}
