﻿using System.Diagnostics;
using System.Windows;
using MainClient.ViewModel;

namespace MainClient
{
    public partial class App : Application
    {
        protected override void OnExit(ExitEventArgs e)
        {
            var locator = Resources["Locator"] as ViewModelLocator;
            Debug.Assert(locator != null, "locator == null");
            locator.Cleanup();
            base.OnExit(e);
        }
    }
}
