﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using API.Communication.ServiceCallbacks;
using API.Data;
using CommunicationLayer.DataInteraction.Controllers;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Controls.ViewModels.Data;
using Controls.ViewModels.Workspaces;
using GalaSoft.MvvmLight.Command;
using Utils;

namespace Controls.Editors.ClientEditor.ViewModel
{
    public class ClientEditorVm : CollectionWorkspaceBase<ClientVm>, IClientServiceCallback
    {
        private RelayCommand _addClient;
        private RelayCommand _removeClient;
        private ClientVm _selectedClient;
        private RelayCommand<string> _applyRenameScCommand;

        public ClientEditorVm()
        {
            RootElements = new ObservableCollection<ClientVm>();
        }

        public IClientServiceController ClientController { get; set; }

        public ClientVm SelectedClient
        {
            get { return _selectedClient; }
            set
            {
                _selectedClient = value;
                RaisePropertyChanged();
            }
        }

        public RelayCommand AddClientCommand
        {
            get
            {
                return _addClient ?? (_addClient = new RelayCommand(() =>
                {
                    Task.Factory.StartNew(() =>
                    {
                        if (ClientController.IsServiceAlive)
                        {
                            const string template = "Название организации";
                            string[] names = RootElements.Select(cl => cl.Name).ToArray();
                            string newName = new NameGenerator().GenerateNewName(template, names);
                            ClientController.AddClient(new Client(newName, "Неизвестен"));
                        }
                    });
                }));
            }
        }

        /// <summary>
        /// Удалить клиента 
        /// </summary>
        public RelayCommand RemoveClientCommand
        {
            get
            {
                return _removeClient ?? (_removeClient = new RelayCommand(() =>
                {
                    if (SelectedClient != null)
                    {
                        if (ClientController.IsServiceAlive)
                        {
                            ClientController.RemoveClient(SelectedClient.Id);
                            Debug.WriteLine("Нажали кнопку \"Удалить\". Удаляем: ID = " + SelectedClient.Id);
                        }
                    }
                }));
            }
        }

        public RelayCommand<string> ApplyRenameSelectedClientCommand
        {
            get
            {
                return _applyRenameScCommand ?? (_applyRenameScCommand =
                    new RelayCommand<string>(ApplyRenameSelectedClient));
            }
        }

        private void ApplyRenameSelectedClient(string text)
        {
            ClientVm client = SelectedClient;
            if (client != null && !string.IsNullOrWhiteSpace(text))
            {
                if (ClientController.IsServiceAlive)
                {
                    client.Name = text;

                    Task.Factory.StartNew(() =>
                    {
                        ClientController.UpdateClient(client.ModelCopy);
                    });
                }
            }
        }

        public void Clear()
        {
            RootElements.Clear();
        }

        public async void ClientAdded(int id)
        {
            if (ClientController.IsServiceAlive)
            {
                var client = await Task<Client>.Factory.StartNew(() => ClientController.GetClientById(id));
                ClientVm clientVm = new ClientVm(client);
                RootElements.Add(clientVm);
            }
        }

        public void ClientRemoved(int clientId)
        {
            var client = RootElements.FirstOrDefault(c => c.Id == clientId);
            if (client != null)
            {
                RootElements.Remove(client);
                Debug.WriteLine("Пришёл колбек \"Клиент удалён\". Удаляем: ID = " + client.Id);
                RecountSelectedIndex();
            }
        }

        public async void ClientUpdated(int id)
        {
            if (ClientController.IsServiceAlive)
            {
                var client = await Task<Client>.Factory.StartNew(() => ClientController.GetClientById(id));

                var clientVm = RootElements.FirstOrDefault(c => c.Id == id);
                if (clientVm != null)
                {
                    clientVm.UpdateModel(client);
                }
            }
        }

        public async void UpdateClients()
        {
            if (ClientController.IsServiceAlive)
            {
                var clients = await Task<List<Client>>.Factory.StartNew(() => ClientController.GetAllClients().ToList());
                RootElements = new ObservableCollection<ClientVm>(clients.Select(c => new ClientVm(c)));

            //    //Удалить клиентов, которые были удалены на другом клиенте, пока сервис клиентов был отключен
            //    if (RootElements.Count > 0)
            //    {
            //        var clientsFromRoot = RootElements.Select(clVm => clVm.Model);
            //        var exceptedClients = clientsFromRoot
            //            .Except(clients, EqualityComparer.ByClientId);
            //        foreach (Client client in exceptedClients)
            //        {
            //            var clientForRemoving = RootElements.FirstOrDefault(c => c.Id == client.Id);
            //            if (clientForRemoving != null)
            //            {
            //                RootElements.Remove(clientForRemoving);
            //            }
            //        }
            //    }

            //    //Обновить ранее добавленных клиентов
            //    foreach (ClientVm clientVm in RootElements)
            //    {
            //        for (int i = 0; i < clients.Count; i++)
            //        {
            //            Client client = clients[i];
            //            if (clientVm.Id == client.Id)
            //            {
            //                clientVm.UpdateModel(client);
            //                clients.Remove(client);
            //                i--;
            //            }
            //        }
            //    }
            //    //-----------------------------------//

            //    foreach (Client client in clients)
            //    {
            //        RootElements.Add(new ClientVm(client));
            //    }
            }
        }
    }
}
