﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using API.Communication.ServiceCallbacks;
using API.Data;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Controls.ViewModels.Data;
using Controls.ViewModels.Workspaces;
using GalaSoft.MvvmLight.Command;

namespace Controls.Editors.CarEditor.ViewModel
{
    /// <summary>
    /// Модель представления редактора автомобилей
    /// </summary>
    public class CarEditorVm : CollectionWorkspaceBase<CarVm>, ICarServiceCallback
    {
        private RelayCommand _addClient;
        private RelayCommand _removeClient;
        private CarVm _selectedCar;

        public CarEditorVm()
        {
            RootElements = new ObservableCollection<CarVm>();
        }

        public ICarServiceController CarController { get; set; }

        public CarVm SelectedCar
        {
            get { return _selectedCar; }
            set
            {
                _selectedCar = value;
                RaisePropertyChanged();
            }
        }


        public RelayCommand AddCarCommand
        {
            get
            {
                return _addClient ??
                       (_addClient =
                           new RelayCommand(
                               () => { Task.Factory.StartNew(() => { CarController.AddCar(new Car("Машина")); }); }));
            }
        }

        public RelayCommand RemoveCarCommand
        {
            get
            {
                return _removeClient ?? (_removeClient = new RelayCommand(() =>
                {
                    if (SelectedCar != null)
                    {
                        RootElements.Remove(SelectedCar);
                        RecountSelectedIndex();
                    }
                }));
            }
        }

        public async void CarAdded(int id)
        {
            if (CarController.IsServiceAlive)
            {
                var car = await Task<Car>.Factory.StartNew(() => CarController.GetCarById(id));
                RootElements.Add(new CarVm(car));
            }
        }

        public void CarRemoved(int id)
        {
            var car = RootElements.FirstOrDefault(c => c.Id == id);
            if (car != null)
            {
                RootElements.Remove(car);
                Debug.WriteLine("Пришёл колбек \"Автомобиль удалён\". Удаляем: ID = " + car.Id);
                RecountSelectedIndex();
            }
        }

        public async void CarUpdated(int id)
        {
            if (CarController.IsServiceAlive)
            {
                var car = await Task<Car>.Factory.StartNew(() => CarController.GetCarById(id));

                var carVm = RootElements.FirstOrDefault(c => c.Id == id);
                if (carVm != null)
                {
                    carVm.UpdateModel(car);
                }
            }
        }

        public void Clear()
        {
            RootElements.Clear();
        }
    }
}