using API.Comparators;

namespace Controls
{
    public static class EqualityComparer
    {
        static EqualityComparer()
        {
            ByClientId = new EqualityComparerByClientId();
        }

        public static EqualityComparerByClientId ByClientId { get; private set; }
    }
}