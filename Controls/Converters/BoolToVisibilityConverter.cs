﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Controls.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public class BoolToVisibilityConverter : IValueConverter
    {
        private bool _invert;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter != null)
            {
                Visibility visibility;

                if (bool.TryParse((string)parameter, out _invert))
                {
                    if (_invert)
                        return (bool)value ? Visibility.Collapsed : Visibility.Visible;
                }
                else if (Enum.TryParse((string)parameter, true, out visibility))
                {
                    return (bool)value ? Visibility.Visible : visibility;
                }
            }

            return (bool)value ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
