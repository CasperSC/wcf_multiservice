﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Controls.Converters
{
    [ValueConversion(typeof(object), typeof(bool))]
    public class IntValueToEnabledConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int paramValue;
            if (int.TryParse((string)parameter, out paramValue))
            {
                return (int)value == paramValue;
            }

            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}