using System;
using System.Globalization;
using System.Windows.Data;

namespace Controls.Converters
{
    [ValueConversion(typeof(int), typeof(bool))]
    public class NumberWithConditionToBoolConverter : IValueConverter
    {
        /// <summary>������ ���</summary>
        public const string MoreThan = ">";
        /// <summary>������ ���</summary>
        public const string LessThan = "<";
        /// <summary>�����</summary>
        public const string Equals = "==";
        /// <summary>�� �����</summary>
        public const string NotEquals = "!=";
        /// <summary>������ ���� �����</summary>
        public const string GreaterThanOrEqual = ">=";
        /// <summary>������ ���� �����</summary>
        public const string LessThanOrEqual = "<=";

        /// <summary>
        /// ����������� ��������.
        /// </summary>
        /// <returns>
        /// ��������������� ��������. ���� ����� ���������� null, ������������ �������������� �������� null.
        /// </returns>
        /// <param name="value">��������, ������������� �������� ���������.</param><param name="targetType">��� �������� ���� ����������.</param><param name="parameter">������������ �������� ���������������.</param><param name="culture">���� � ������������ ���������, ������������ � ���������������.</param>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is int && parameter is string)
            {
                int number;
                string sign;

                if (TryGetSignWithValue((string)parameter, out sign, out number))
                {
                    int compareValue = (int)value;

                    switch (sign)
                    {
                        // ">="
                        case GreaterThanOrEqual:
                            return compareValue >= number;

                        // "<="
                        case LessThanOrEqual:
                            return compareValue <= number;

                        // ">"
                        case MoreThan:
                            return compareValue > number;

                        // "<";
                        case LessThan:
                            return compareValue < number;

                        // "=="
                        case Equals:
                            return compareValue == number;

                        // "!="
                        case NotEquals:
                            return compareValue != number;

                        default:
                            return false;
                    }
                }

            }

            return false;
        }

        /// <summary>
        /// ����������� ��������.
        /// </summary>
        /// <returns>
        /// ��������������� ��������. ���� ����� ���������� null, ������������ �������������� �������� null.
        /// </returns>
        /// <param name="value">��������, ������������� ����� ��������.</param><param name="targetType">���, � ������� ����������� ��������������.</param><param name="parameter">������������ �������� ���������������.</param><param name="culture">���� � ������������ ���������, ������������ � ���������������.</param>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }


        private bool TryGetSignWithValue(string parameter, out string sign, out int number)
        {
            sign = string.Empty;
            number = 0;

            if (parameter.StartsWith(GreaterThanOrEqual))   // ">="
            {
                sign = GreaterThanOrEqual;
            }
            else if (parameter.StartsWith(LessThanOrEqual)) // "<="
            {
                sign = LessThanOrEqual;
            }
            else if (parameter.StartsWith(Equals)) // "=="
            {
                sign = Equals;
            }
            else if (parameter.StartsWith(NotEquals)) // "!="
            {
                sign = NotEquals;
            }
            else if (parameter.StartsWith(MoreThan)) // ">"
            {
                sign = MoreThan;
            }
            else if (parameter.StartsWith(LessThan)) // "<"
            {
                sign = LessThan;
            }

            if (sign != string.Empty && parameter.Length > sign.Length)
            {
                string substring = parameter.Substring(sign.Length, parameter.Length - sign.Length);
                return int.TryParse(substring, out number);
            }

            return false;
        }
    }
}