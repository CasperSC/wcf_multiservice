﻿using System;
using API;
using API.Data;
using GalaSoft.MvvmLight;

namespace Controls.ViewModels.Data
{
    /// <summary>
    /// Модель предсталение для класса <see cref="Client" />
    /// </summary>
    public class ClientVm : ViewModelBase, IModelUpdater<Client>, IModelWrapper<Client>
    {
        private Client _client;

        public ClientVm(Client client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }
            _client = client;
        }

        /// <summary>id клиента</summary>
        public int Id
        {
            get { return _client.Id; }
            set
            {
                _client.Id = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>Имя клиента</summary>
        public string Name
        {
            get { return _client.Name; }
            set
            {
                _client.Name = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>Адрес клиента</summary>
        public string Address
        {
            get { return _client.Address; }
            set
            {
                _client.Address = value;
                RaisePropertyChanged();
            }
        }

        public void UpdateModel(Client model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            _client = model;
            RaisePropertyChanged("Id");
            RaisePropertyChanged("Name");
            RaisePropertyChanged("Address");
        }

        /// <summary>
        /// Возвращает копию модели <see cref="Client"/>
        /// </summary>
        public Client ModelCopy
        {
            get { return _client.Clone(); }
        }
    }
}