﻿using System;
using API;
using API.Data;
using GalaSoft.MvvmLight;

namespace Controls.ViewModels.Data
{
    /// <summary>
    /// Модель предсталение для класса <see cref="Car"/>
    /// </summary>
    public class CarVm : ViewModelBase, IModelUpdater<Car>, IModelWrapper<Car>
    {
        private Car _car;

        public CarVm(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException("car");
            }
            _car = car;
        }

        public int Id
        {
            get { return _car.Id; }
            set { _car.Id = value; }
        }

        /// <summary>Номер машины</summary>
        public string Number
        {
            get { return _car.Number; }
            set { _car.Number = value; }
        }

        public void UpdateModel(Car model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            _car = model;
            RaisePropertyChanged("Id");
            RaisePropertyChanged("Number");
        }

        #region Implementation of IModelWrapper<out Car>

        /// <summary>
        /// Возвращает копию модели <see cref="Car"/>
        /// </summary>
        public Car ModelCopy
        {
            get { return _car.Clone(); }
        }

        #endregion
    }
}