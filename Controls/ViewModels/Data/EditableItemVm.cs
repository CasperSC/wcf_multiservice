﻿using GalaSoft.MvvmLight;

namespace Controls.ViewModels.Data
{
    /// <summary>
    /// Содержит свойство, предназначенное для уведомления, что объект
    /// был отредактирован, но данные не были сохранены.
    /// </summary>
    public class EditableItemVm : ViewModelBase
    {
        private bool _wasEditedWithoutSaving;

        public bool WasEditedWithoutSaving
        {
            get { return _wasEditedWithoutSaving; }
            set
            {
                _wasEditedWithoutSaving = value;
                RaisePropertyChanged();
            }
        }

        protected void MarkAsEdited()
        {
            if (!WasEditedWithoutSaving)
            {
                WasEditedWithoutSaving = true;
            }
        }
    }
}