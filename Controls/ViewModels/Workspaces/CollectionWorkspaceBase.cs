using System.Collections.ObjectModel;
using System.Diagnostics;
using GalaSoft.MvvmLight;

namespace Controls.ViewModels.Workspaces
{
    public class CollectionWorkspaceBase<T> : ViewModelBase
    {
        private int _elementsSelectedIndex;
        private int _lastElementsSelectedIndex;
        private ObservableCollection<T> _rootElements;


        /// <summary>������� ������ ���������� �������� � ��������� ������</summary>
        public int ElementsSelectedIndex
        {
            get { return _elementsSelectedIndex; }
            set
            {
                LastElementsSelectedIndex = _elementsSelectedIndex;
                Debug.WriteLine("ElementsSelectedIndex = " + value);
                _elementsSelectedIndex = value;
                RaisePropertyChanged();
            }
        }

        /// <summary>���������� ������ ���������� �������� � ��������� ������</summary>
        private int LastElementsSelectedIndex
        {
            get { return _lastElementsSelectedIndex; }
            set
            {    
                _lastElementsSelectedIndex = value;
                Debug.WriteLine("LastElementsSelectedIndex = " + value);
            }
        }

        protected void RecountSelectedIndex()
        {
            if (LastElementsSelectedIndex > -1 && RootElements.Count > LastElementsSelectedIndex)
            {
                ElementsSelectedIndex = LastElementsSelectedIndex;
            }
            else if (LastElementsSelectedIndex > 0 && RootElements.Count > 0)
            {
                ElementsSelectedIndex = LastElementsSelectedIndex - 1;
            }
        }

        /// <summary>
        /// �������� �������� ������, ������������ � ������
        /// </summary>
        public ObservableCollection<T> RootElements
        {
            get { return _rootElements; }
            protected set
            {
                _rootElements = value;
                RaisePropertyChanged();
            }
        }
    }
}