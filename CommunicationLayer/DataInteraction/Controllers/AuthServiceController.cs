using System;
using System.Diagnostics;
using System.ServiceModel;
using System.ServiceModel.Channels;
using API.Communication.Auth;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Utils.Logging;
using XXX.API.Communication.Auth;

namespace CommunicationLayer.DataInteraction.Controllers
{
    public sealed class AuthServiceController : DuplexServiceControllerBase<IAuthService, IAuthServiceCallback>, IAuthServiceController
    {
        public AuthServiceController(ILogger logger)
        {
            Debug.Assert(logger != null, "logger == null");
            AddLoggerInternal(logger);
        }

        public AuthResult Login(AuthData authData)
        {
            if (_service == null)
                throw new NullReferenceException("_service");

            try
            {
                return _service.Login(authData);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel)_service);
            }
            return null;
        }

        public void Logout()
        {
            try
            {
                if (_service != null)
                {
                    _service.Logout();
                    CloseChannel((IContextChannel)_service);
                    _service = null;
                }
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel)_service);
            }
        }

        public bool CheckConnection()
        {
            try
            {
                return _service.CheckConnection();
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel)_service);
            }
            return false;
        }

        public bool RegisterUser(string login, string password)
        {
            try
            {
                return _service.RegisterUser(login, password);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel)_service);
            }
            return false;
        }

        public bool SetPermissionLevel(string login, Permission permission)
        {
            try
            {
                return _service.SetPermissionLevel(login, permission);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel)_service);
            }
            return false;
        }
    }
}