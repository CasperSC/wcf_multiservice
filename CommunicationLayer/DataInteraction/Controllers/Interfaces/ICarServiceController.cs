using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using API.Data;

namespace CommunicationLayer.DataInteraction.Controllers.Interfaces
{   
    /// <summary>
    /// ������������ ��������� ��� ������ � ��������, ��������������� ��� �������������� � ������������
    /// </summary>
    public interface ICarServiceController : ICallbackBinder, IDuplexServiceControllerBase<ICarService, ICarServiceCallback>
    {
        bool AddCar(Car car);
        Car GetCarById(int id);
    }
}