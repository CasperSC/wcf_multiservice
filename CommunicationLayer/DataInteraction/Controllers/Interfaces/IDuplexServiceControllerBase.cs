using System;
using API.Communication;
using Utils.Logging;

namespace CommunicationLayer.DataInteraction.Controllers.Interfaces
{
    /// <summary>
    /// ������������ ��������� ��� ������ � ���������� ��������, ��������� ������������ �����������
    /// </summary>
    /// <typeparam name="TServiceCallback"></typeparam>
    /// <typeparam name="TService"></typeparam>
    public interface IDuplexServiceControllerBase<TService, TServiceCallback>
    {
        /// <summary>
        /// ����������, ����� ��������� ����������� ��������.
        /// </summary>
        event EventHandler<bool> ConnectionStateChanged;

        /// <summary>
        /// ���������� ������ ���������� � ��������, ���������� true, ���� ���������� �����������
        /// </summary>
        bool IsServiceAlive { get; }

        /// <summary>
        /// ����� ��� ��������� ���������� � ��������.
        /// </summary>
        string Address { get; set; }

        TService Service { get; }
        /// <summary>
        /// ����������, ����� ������������ �����-���� ���������� ��� ��������� � �������
        /// </summary>
        event EventHandler<ServiceExceptionEventArgs> ServiceThrewAnException;

        void CloseService();

        void AddLogger(ILogger logger);

        void InitializeService(TServiceCallback callback);

        void InitializeService(string address, TServiceCallback callback);
    }
}