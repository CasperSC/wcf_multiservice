using XXX.API.Communication.Auth;

namespace CommunicationLayer.DataInteraction.Controllers.Interfaces
{
    public interface ICallbackBinder
    {
        /// <summary>
        /// ����������� �� ������
        /// </summary>
        /// <param name="auth"></param>
        /// <returns></returns>
        bool SubscribeCallback(AuthResult auth);
    }
}