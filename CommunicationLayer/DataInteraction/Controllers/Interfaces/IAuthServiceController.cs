using API.Communication.Auth;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using XXX.API.Communication.Auth;

namespace CommunicationLayer.DataInteraction.Controllers.Interfaces
{
    /// <summary>
    /// ������������ ��������� ��� ������ � �������� �����������
    /// </summary>
    public interface IAuthServiceController : IDuplexServiceControllerBase<IAuthService, IAuthServiceCallback>
    {
        AuthResult Login(AuthData authData);
        void Logout();
        bool CheckConnection();
        bool RegisterUser(string login, string password);
        bool SetPermissionLevel(string login, Permission permission);
    }
}