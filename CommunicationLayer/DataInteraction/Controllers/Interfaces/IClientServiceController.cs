using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using API.Data;

namespace CommunicationLayer.DataInteraction.Controllers.Interfaces
{    
    /// <summary>
    /// ������������ ��������� ��� ������ � ��������, ��������������� ��� �������������� � ���������
    /// </summary>
    public interface IClientServiceController : ICallbackBinder, IDuplexServiceControllerBase<IClientService, IClientServiceCallback>
    {
        bool AddClient(Client client);
        bool RemoveClient(int id);
        Client[] GetAllClients();
        Client GetClientById(int id);
        bool UpdateClient(Client client);
    }
}