using System;
using System.Diagnostics;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using API.Data;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using XXX.API.Communication.Auth;

namespace CommunicationLayer.DataInteraction.Controllers
{
    public sealed class ClientServiceController : DuplexServiceControllerBase<IClientService, IClientServiceCallback>,
        IClientServiceController
    {
        public bool AddClient(Client client)
        {
            try
            {
                return _service.Add(client);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return false;
        }

        public bool RemoveClient(int id)
        {
            try
            {
                return _service.RemoveClient(id);
            }
            catch (FaultException ex)
            {
                Debug.WriteLine(ex.Action);
                HandleException(ex, (IContextChannel) _service);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }

            return false;
        }

        public Client[] GetAllClients()
        {
            try
            {
                return _service.GetAll();
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return null;
        }

        public Client GetClientById(int id)
        {
            try
            {
                return _service.GetById(id);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return null;
        }

        public bool UpdateClient(Client client)
        {
            try
            {
                return _service.Update(client);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return false;
        }

        public bool SubscribeCallback(AuthResult auth)
        {
            try
            {
                return _service.Connect(auth.User.Id, auth.Token);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return false;
        }
    }
}