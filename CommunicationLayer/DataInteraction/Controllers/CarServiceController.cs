using System;
using System.ServiceModel;
using API.Communication.ServiceCallbacks;
using API.Communication.Services;
using API.Data;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using XXX.API.Communication.Auth;

namespace CommunicationLayer.DataInteraction.Controllers
{
    public sealed class CarServiceController : DuplexServiceControllerBase<ICarService, ICarServiceCallback>, ICarServiceController
    {
        public bool AddCar(Car car)
        {
            try
            {
                return _service.Add(car);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return false;
        }

        public Car GetCarById(int id)
        {
            try
            {
                return _service.GetById(id);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return null;
        }

        public bool SubscribeCallback(AuthResult auth)
        {
            try
            {
                return _service.Connect(auth.User.Id, auth.Token);
            }
            catch (Exception ex)
            {
                HandleException(ex, (IContextChannel) _service);
            }
            return false;
        }
    }
}