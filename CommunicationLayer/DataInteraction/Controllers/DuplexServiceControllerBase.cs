using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.ServiceModel;
using System.Threading;
using API.Communication;
using CommunicationLayer.DataInteraction.Controllers.Interfaces;
using Utils.Logging;

namespace CommunicationLayer.DataInteraction.Controllers
{
    /// <summary>
    /// ������� ���������� � ���������� ������.
    /// </summary>
    /// <typeparam name="TService">������</typeparam>
    /// <typeparam name="TServiceCallback">������ ��� �������</typeparam>
    public abstract class DuplexServiceControllerBase<TService, TServiceCallback> :
        IDuplexServiceControllerBase<TService, TServiceCallback>
        where TServiceCallback : class
    {
        private readonly List<ILogger> _loggers;
        private readonly SynchronizationContext _syncContext;
        protected TService _service;

        protected DuplexServiceControllerBase()
        {
            _syncContext = SynchronizationContext.Current;
            _loggers = new List<ILogger>();
        }

        /// <summary>
        /// ����������, ����� ��������� ���������� ��������.
        /// </summary>
        public event EventHandler<bool> ConnectionStateChanged;

        /// <summary>
        /// ����������, ����� ������������ �����-���� ���������� ��� ��������� � �������
        /// </summary>
        public event EventHandler<ServiceExceptionEventArgs> ServiceThrewAnException;

        public bool IsServiceAlive
        {
            get
            {
                IContextChannel channel = (IContextChannel) _service;
                return _service != null && channel.State == CommunicationState.Opened;
            }
        }

        public string Address { get; set; }

        public TService Service
        {
            get { return _service; }
        }

        /// <summary>
        /// ������������������� ������
        /// </summary>
        /// <param name="callback"></param>
        public void InitializeService(TServiceCallback callback)
        {
            InitializeServiceInternal(Address, callback);
        }

        public void InitializeService(string address, TServiceCallback callback)
        {
            InitializeServiceInternal(address, callback);
        }

        public void CloseService()
        {
            CloseChannel((IContextChannel) _service);
        }

        public void AddLogger(ILogger logger)
        {
            AddLoggerInternal(logger);
        }

        protected void OnConnectionStateChanged(bool state)
        {
            EventHandler<bool> handler = ConnectionStateChanged;
            if (handler != null)
            {
                handler(this, state);
            }
        }

        protected void SubscribeToChannelEvents()
        {
            IContextChannel channel = (IContextChannel) _service;
            channel.Opened += Channel_Opened;
            channel.Closed += Channel_Closed;
        }

        protected void UnsubscribeFromChannelEvents()
        {
            IContextChannel channel = (IContextChannel) _service;
            channel.Opened -= Channel_Opened;
            channel.Closed -= Channel_Closed;
        }

        private void Channel_Closed(object sender, EventArgs e)
        {
            OnConnectionStateChanged(false);

            IContextChannel channel = (IContextChannel) sender;
            channel.Opened -= Channel_Opened;
            channel.Closed -= Channel_Closed;
        }

        private void Channel_Opened(object sender, EventArgs e)
        {
            OnConnectionStateChanged(true);
        }

        protected void InitializeServiceInternal(string address, TServiceCallback callback)
        {
            Address = address;
            _service = InitServiceInternal(callback);
            SubscribeToChannelEvents();
        }

        protected void CloseChannel(IContextChannel channel)
        {
            if (channel != null)
            {
                channel.Close();
                UnsubscribeFromChannelEvents();
            }
        }

        protected TService InitServiceInternal(TServiceCallback callback)
        {
            if (string.IsNullOrWhiteSpace(Address))
            {
                throw new NullReferenceException("Property Address is null or white space");
            }
            EndpointAddress endpoint = new EndpointAddress(new Uri(Address));
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None);
            TService service = DuplexChannelFactory<TService>.CreateChannel(callback, binding, endpoint);
#if DEBUG
            binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
            binding.SendTimeout = TimeSpan.FromMinutes(10);
#endif
            return service;
        }

        protected void AddLoggerInternal(ILogger logger)
        {
            _loggers.Add(logger);
        }

        protected void AddLoggersInternal(IEnumerable<ILogger> loggers)
        {
            _loggers.AddRange(loggers);
        }

        protected void HandleException(Exception ex, IContextChannel channel, bool debugFailShow = true)
        {
#if DEBUG
            Debug.WriteLine(ex.Message);
            if (debugFailShow)
                Debug.Fail(ex.Message);
#endif

            foreach (ILogger logger in _loggers)
            {
                logger.AppendException(ex);
            }

            OnServiceThrewAnException(ex);
        }

        protected void OnServiceThrewAnException(Exception ex)
        {
            EventHandler<ServiceExceptionEventArgs> handler = ServiceThrewAnException;
            if (handler != null)
            {
                _syncContext.Post(
                    exception => { handler(this, new ServiceExceptionEventArgs((Exception) exception)); }, ex);
            }
        }
    }
}