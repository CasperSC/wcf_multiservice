﻿using System;
using Utils.Logging;

namespace Utils
{
    public class NameGenerator : INameGenerator
    {
        /// <summary>
        /// Сгенерировать имя 
        /// </summary>
        /// <param name="nameTemplate">Текст, который будет в качестве шаблона</param>
        /// <param name="existsNames">Список существующих имён</param>
        /// <returns></returns>
        public string GenerateNewName(string nameTemplate, string[] existsNames)
        {
            int counter = 1;

            Array.Sort(existsNames);

            for (int i = 0; i < existsNames.Length; i++)
            {
                for (int j = 0; j < existsNames.Length; j++)
                {
                    string recipeName = string.Format("{0} {1}", nameTemplate, counter);
                    if (existsNames[j] == recipeName)
                    {
                        counter++;
                    }
                }
            }

            return string.Format("{0} {1}", nameTemplate, counter);
        }
    }
}