﻿using System.Security.Cryptography;
using System.Text;

namespace Utils.Security
{
    public class HashHelper
    {
        /// <summary>
        /// Получить Md5 хэш из строки длинной 32 символа
        /// </summary>
        /// <param name="text">Целевая строка, из которой будет получен хэш</param>
        /// <returns></returns>
        public static string ComputeMd5Hash(string text)
        {
            MD5 md5Hasher = MD5.Create();
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(text));

            var sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
    }
}
