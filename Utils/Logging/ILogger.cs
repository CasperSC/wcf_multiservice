﻿using System;

namespace Utils.Logging
{
    public interface ILogger
    {
        /// <summary>
        /// Добавить текст в конец файла лога, добавив в начале дату и время с точностью до миллесекунд в начале текста
        /// </summary>
        /// <param name="text"></param>
        void AppendText(string text);

        void AppendException(Exception ex);
    }

    public interface IFileLogger : ILogger
    {
        /// <summary>Путь к папке содержащей логи приложений</summary>
        string LogPath { get; set; }
    }
}