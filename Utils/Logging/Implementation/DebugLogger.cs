﻿using System;
using System.Diagnostics;
using Utils.Interfaces;

namespace Utils.Logging.Implementation
{
    public class DebugLogger : ILogger
    {
        private readonly IExceptionFormatter _exceptionFormatter;

        public DebugLogger(IExceptionFormatter exceptionFormatter)
        {
            if (exceptionFormatter == null)
            {
                throw new ArgumentNullException("exceptionFormatter");
            }
            _exceptionFormatter = exceptionFormatter;
        }

        /// <summary>
        /// Добавить текст в конец файла лога, добавив в начале дату и время с точностью до миллесекунд в начале текста
        /// </summary>
        /// <param name="text"></param>
        public void AppendText(string text)
        {
            Debug.WriteLine(text);
        }

        public void AppendException(Exception ex)
        {
            Debug.WriteLine(_exceptionFormatter.Format(ex));
        }
    }
}