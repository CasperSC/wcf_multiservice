﻿using System;

namespace Utils.Logging.Implementation
{
    public class EmptyLogger : ILogger
    {
        /// <summary>
        /// Добавить текст в конец файла лога, добавив в начале дату и время с точностью до миллесекунд в начале текста
        /// </summary>
        /// <param name="text"></param>
        public void AppendText(string text)
        {
        }

        public void AppendException(Exception ex)
        {
        }
    }
}
