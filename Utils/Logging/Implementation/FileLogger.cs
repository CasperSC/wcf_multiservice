﻿using System;
using System.IO;

namespace Utils.Logging.Implementation
{
    public class FileLogger : IFileLogger
    {
        private static readonly object _appendExceptionLocker = new object();
        private static readonly object _lockerObject = new object();
        private const string _separator = "=========================";

        public FileLogger(string logPath, string fileName)
        {
            if (!Directory.Exists(logPath))
                Directory.CreateDirectory(logPath);

            LogPath = logPath;

            if (string.IsNullOrWhiteSpace(fileName))
                throw new ArgumentException("fileName");


            FileName = fileName;

            FullPathToLastWritedLogFile = Path.Combine(logPath, fileName);
        }

        /// <summary>Путь к папке содержащей логи приложений</summary>
        public string LogPath { get; set; }

        /// <summary>Начальное название файла</summary>
        public string FileName { get; set; }

        /// <summary>Полный путь к файлу конфигурации, который был открыт для записи последний раз.
        /// Каждую новую запись это свойство обновляется</summary>
        public string FullPathToLastWritedLogFile { get; private set; }

        /// <summary>
        /// Добавить текст в конец файла лога, добавив в начале дату и время с точностью до миллесекунд в начале текста
        /// </summary>
        /// <param name="text"></param>
        public void AppendText(string text)
        {
            lock (_lockerObject)
            {
                DateTime now = DateTime.Now;
                string resultText = string.Format("{0} {1}: {3}{2}{3}{4}{3}{3}",
                    now.ToShortDateString(),
                    now.TimeOfDay,
                    text,
                    Environment.NewLine,
                    _separator);

                FullPathToLastWritedLogFile = Path.Combine(LogPath,
                    string.Format("{0} - {1}{2}", FileName, now.ToShortDateString(), ".txt"));
                File.AppendAllText(FullPathToLastWritedLogFile, resultText);
            }
        }

        public void AppendException(Exception ex)
        {
            lock (_appendExceptionLocker)
            {
                string text = string.Format(
                    "Message: {1};{0}" +
                    "Source: {2};{0}" +
                    "TargetSite: {3}{0}" +
                    "InnerException: {4};{0}" +
                    "StackTrace: {0}{5};"
                    , Environment.NewLine, ex.Message, ex.Source, ex.TargetSite, ex.InnerException, ex.StackTrace);
                AppendText(text);
            }
        }
    }
}