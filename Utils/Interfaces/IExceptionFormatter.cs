﻿using System;

namespace Utils.Interfaces
{
    /// <summary>
    /// Интерфейс, предоставляющий доступ к методу форматирования исключения в строку
    /// </summary>
    public interface IExceptionFormatter
    {
        /// <summary>
        /// Форматировать исключение в строку
        /// </summary>
        /// <param name="ex">Исключение</param>
        /// <returns></returns>
        string Format(Exception ex);
    }
}
