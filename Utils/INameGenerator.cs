﻿namespace Utils
{
    public interface INameGenerator
    {
        /// <summary>
        /// Сгенерировать имя 
        /// </summary>
        /// <param name="nameTemplate">Текст, который будет в качестве шаблона</param>
        /// <param name="existsNames">Список существующих имён</param>
        /// <returns></returns>
        string GenerateNewName(string nameTemplate, string[] existsNames);
    }
}